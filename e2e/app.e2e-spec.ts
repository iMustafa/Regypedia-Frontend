import { RegypediaFrontendPage } from './app.po';

describe('regypedia-frontend App', () => {
  let page: RegypediaFrontendPage;

  beforeEach(() => {
    page = new RegypediaFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
