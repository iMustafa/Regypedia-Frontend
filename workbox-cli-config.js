module.exports = {
  "globDirectory": "dist/",
  "globPatterns": [
    "**/*.{jpg,otf,css,png,ico,html,js,map,json,bundle}"
  ],
  "swDest": "./dist/service-worker.js",
  "globIgnores": [
    "../workbox-cli-config.js"
  ],
  "manifestDest": "./dist/manifest.js",
  "maximumFileSizeToCacheInBytes": 5633508,
  // "skipWaiting":true
};
//const networkFirstStrategy = workboxSW.strategies.networkFirst();
//workboxSW.router.registerRoute('https://panel.cmcserv.com/panel/api', networkFirstStrategy);
