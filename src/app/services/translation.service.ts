import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CookiesService } from './cookies.service';

@Injectable()
export class TranslationService {

  constructor(public  cookies: CookiesService) {
    this.getLang();
  }

  public langKey: string;
  public currentLang: any;
  public direction: string;
  public fontFamily: string;
  public floatValue: string;
  public textAlignValue: string;
  public leftPositionValue: string;
  public menuFloatValue: string;
  public innerWidth: number;

  public en: Object = {
    register: 'Register',
    demoReq: 'Request a Demo',
    name: 'Name',
    firstName: 'First Name',
    lastName: 'Last Name',
    email: 'Email',
    password: 'Password',
    companyName: 'Company Name',
    phoneNumber: 'Phone Number',
    function: 'Function of Company',
    logo: 'Logo',
    mainPage: 'Home',
    aboutUs: 'About Us',
    services: 'Services',
    contactUs: 'Contact Us',
    userLogin: 'Login',
    userLogout: 'Logout',
    forgetPassword: 'Forgot password?',
    resetPassword: 'Reset password',
    reset: 'Reset',
    apply: 'Applies',
    noApply: 'Doesn\'t Apply',
    langBtn: 'AR',
    filter: 'Filter',
    keywords: 'Keywords',
    archive: 'Archive',
    home: 'Home',
    myregulations: 'Regulations',
    results: 'Results',
    favorite: 'Favorite',
    regType: 'Type',
    regNum: 'Regulation number',
    regYear: 'Year',
    regName: 'Name',
    articleNum: 'Article Number',
    articles: 'Articles',
    search: 'Enter Search Keyword ..',
    attachment: 'Attachment',
    confirmPurchase: 'Purchase this regulation for',
    points: 'points',
    questionMark: '?',
    questions: 'Questions',
    appliedReg: 'Applied Regulations',
    showResults: 'Show Results',
    confirm: 'Confirm',
    cancel: 'Cancel',
    relatedReg: 'Related Regulations',
    open: 'Open',
    keySearch: 'Search by keywords ..',
    freeSearch: 'Free search..',
    tools: 'Tools',
    choose_date: 'Choose the notification date',
    enterCompliancePlan: 'Enter Compliance Plan ..',
    CompliancePlan: 'Compliance Plan',
    library: 'My library',
    unarchive: 'Unarchive',
    unfavorite: 'Unfavorite',
    vote : 'Vote',
    noThingToShow : 'No thing to show',
    searchResults : 'No Search Results',
    internet: 'Internent connction error',
    loading: 'Loading',
    youCanAddMore : 'You can find and add new regulations by searching form the ',
    print : 'Print',
    page404 : 'Page Not Found',
    dontBeSad : 'Don\'t be sad , life is\'t on your side today , but it will be tomorrow.',
    wannaAdd : 'Add to Library',
    advanceddSearch : 'Advanced Search',
    autoSave: 'Saved',
    notifications: 'Notifications',
    next: 'Next',
    prev: 'Prev',
    error: "Error",
    waiting: "Waiting",
    saving: "Saving",
    saved: "Saved",
    artNum: "Article Number",
    english: "English",
    arabic: "Arabic",
    addDesc: "Add description",
    notificationDate: "Notification Date",
    profile: 'My Profile',
    date : 'Date',
    all: 'All',
    permitName: 'Permit Name' ,
    permitAuthority: 'Approval Authority' ,
    permitPeriod: 'Issuance Period',
    permitValidityPeriod: 'Validity Period',
    permitCost: 'Cost',
    requiredDocuments: 'Required Documents',
    penalties: 'Penalties' ,
    permits : 'Permits',
    translatedText : 'Translated Text',
    text: 'Text',
    help: 'Help',
    settings: 'Settings',
    changeBackground: 'Change Background',
    note: 'Note',
    learnMore: 'Learn More',
    key: 'Confirmation Key',
    resetpasswrd: 'Reset Password',
    sent: 'Sent Successfully',
    textStatus: 'Text Status',
    sending: 'sending...',
    noEmail: 'There no user for this Email',
    msg: 'Message',
    send: 'Send',
    close: 'Close',
    toSearchPage: 'Search',
    toQuestionsPage: 'My Questions',
    toLibraryPage: 'My Library',
  };

  public ar: Object = {
    register: 'التسجيل',
    demoReq: 'التجربة المجانية',
    name: 'الأسم',
    firstName: 'الأسم الأول',
    lastName: 'الأسم العائله',
    email: 'الأيميل',
    password: 'كلمة المرور',
    companyName: 'أسم الشركه',
    phoneNumber: 'رقم الهاتف',
    function: 'مجال عمل الشركه',
    logo: 'الشعار',
    mainPage: 'الرئيسية',
    aboutUs: 'من نحن',
    services: 'خدمات',
    contactUs: 'تواصل معنا',
    userLogin: 'تسجيل الدخول',
    userLogout: 'تسجيل الخروج',
    forgetPassword: 'نسيت كلمة المرور؟',
    resetPassword: 'استرجاع كلمة المرور',
    reset: 'استرجع',
    apply: 'ينطبق',
    noApply: 'لا ينطبق',
    langBtn: 'EN',
    filter: 'فلتر',
    keywords: 'كلمات مفتاحية',
    archive: 'الأرشيف',
    home: 'الرئيسية',
    myregulations: 'المكتبة',
    results: 'نتائج الأختبار',
    favorite: 'المفضلة',
    regType: 'النوع',
    regNum: 'رقم التشريع',
    regYear: 'السنة',
    regName: 'اسم',
    articleNum: 'رقم المادة',
    search: ' أدخل كلمة البحث',
    attachment: 'المرفقات',
    confirmPurchase: 'شراء هذا القانون',
    points: 'نقطه',
    questionMark: '؟',
    questions: 'الأسئلة',
    appliedReg: 'القوانين المنطبقه',
    showResults: 'عرض النتائج',
    confirm: 'تأكيد',
    relatedReg: 'قوانين ذات صله',
    open: 'فتح',
    keySearch: 'أبحث بالكلمة المفتاحية ..',
    freeSearch: 'أبحث في نص المواد ..',
    tools: 'الأدوات',
    choose_date: 'اختر تاريخ التنبيه',
    enterCompliancePlan: 'ادخل خطة الألتزام..',
    CompliancePlan: 'خطة الألتزام',
    library: 'مكتبتي',
    unarchive: 'إرجاع للمكتبة',
    unfavorite: 'إزالة من المفضلة' ,
    vote: 'إرسال ',
    noThingToShow : 'لا يوجد شيء لعرضه',
    searchResults : 'لا يوجد نتائج للبحث',
    internet: ' ربما تكون هناك مشكله في اتصال الإنترنت ',
    loading: 'جاري التحميل',
    youCanAddMore : 'يمكنك إيجاد و اضافة القوانين بالبحث من الصفحة' , 
    print : 'طباعة',
    page404 : 'هذه الصفحة غير موجودة',
    dontBeSad : 'لا تكن حزيناً ، ربما الحياة ليست في صفك اليوم ، لكن تسكون في صفك غداً بالتأكيد.' ,
    wannaAdd : 'إضافة الى مكتبتي',
    advanceddSearch : 'البحث المتقدم',
    autoSave: 'تم الحفظ التلقائي',
    profile: "حسابي",
    notifications: 'الإشعارات',
    next: 'التالي',
    prev: 'السابق',
    error: "خطأ",
    waiting: "انتظار",
    saving: "جار الحفظ",
    saved: "تم الحفظ",
    artNum: "رقم المقال",
    english: "الانجليزيه",
    arabic: "العربيه",
    addDesc: "أضف وصف",
    notificationDate: "تاريخ التنبيه",
    all: 'الكل',
    permitName: 'إسم التصريح' ,
    permitAuthority: 'جهة التصريح' ,
    permitPeriod: 'مدة الإصدار',
    permitValidityPeriod: 'مدة الصلاحية',
    permitCost: 'التكلفة',
    requiredDocuments: 'المستندات المطلوبة',
    penalties: 'العقوبات',
    permits : 'التصاريح',
    articles: 'تشريعات',
    translatedText : 'النص المترجم',
    text: 'النص',
    help: 'مساعدة',
    settings: 'اعدادات',
    changeBackground: 'تغير الخلفية',
    note: 'ملاحظة',
    learnMore: 'اعرف اكثر',
    key: 'رمز التأكيد',
    resetpasswrd: 'تغير كلمة المرور',
    sent: 'تم الأرسال',
    textStatus: 'الحالة النص',
    sending: 'جاري الأرسال',
    noEmail: 'لا يوجد حساب بهذا البريد الألكتروني',
    msg: 'الرسالة',
    send: 'أرسال',
    close: 'إغلاق',
    toSearchPage: 'البحث',
    toQuestionsPage: 'الأسئلة',
    toLibraryPage: 'لمكتبتي',
  };

  public changeLangCookie(): void {
    if (this.cookies.getCookie('lang') === 'ar') {
      this.cookies.setCookie('lang', 'en', 14);
    } else if (this.cookies.getCookie('lang') === 'en') {
      this.cookies.setCookie('lang', 'ar', 14);
    } else {
      this.cookies.setCookie('lang', 'en', 14);
    }
    this.getLang();
  }


  public getLang(): void {
    const lang = this.cookies.getCookie('lang');
    if (lang === 'ar') {
      this.currentLang = this.ar;
      this.langKey = 'ar';
      this.direction = 'rtl';
      this.floatValue = 'left';
      this.menuFloatValue = 'right';
      this.fontFamily = 'GESS-Medium';
      this.textAlignValue = 'right';
      this.leftPositionValue = '12%';
    } else {
      this.cookies.setCookie('lang', 'en', 14);
      this.currentLang = this.en;
      this.langKey = 'en';
      this.direction = 'ltr';
      this.floatValue = 'right';
      this.menuFloatValue = 'left';
      this.fontFamily = 'Helvetica, Roboto, Arial, sans-serif';
      this.textAlignValue = 'left';
      this.leftPositionValue = '80%';
    }
  }

}
