import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthorizationService } from './authorization.service'

@Injectable()
export class TableFilteringService {

  public filterForm: FormGroup;

  constructor(public fb: FormBuilder, public auth: AuthorizationService) {
    this.buildForm();
  }

  public buildForm() {
    this.filterForm = this.fb.group({
      regNumber : [null, Validators.compose([Validators.pattern(/[0-9]/g)])],
      regYear   : [null, Validators.compose([Validators.pattern(/[0-9]/g)])],
      artNumber : [null, Validators.compose([Validators.pattern(/[0-9]/g)])]
    });
  }

  public submitForm($formData) {
    console.log($formData.value);
    var url: string     = "",
        method: string  = "Post",
        data: Object    = {};

    this.auth.sendTokenizedRequest(data, url, method).then(response => {
      console.log(response);
    }).catch(reject => {
      console.log(reject);
    });
  }

}
