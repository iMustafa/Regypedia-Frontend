import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CookiesService } from './cookies.service';
import { AuthorizationService } from './authorization.service';
import { Http } from '@angular/http';
import { Headers, Response, RequestOptions, RequestMethod, URLSearchParams, Request } from '@angular/http';
import 'rxjs';

@Injectable()
export class ActionsService {

  constructor(public  authorize: AuthorizationService, public  cookies: CookiesService, public http: Http) { }

  getActions(): Promise<any> {
    const url = 'https://panel.cmcserv.com/panel/api/v1/user_regulation/getByUserOrdered',
      method = 'Get',
      token = 'api_token';
    return this.authorize.sendTokenizedRequest(null, url, method, token);

  }

}
