import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { AuthorizationService } from './authorization.service';
import { TranslationService } from './translation.service';
import {MdDatepickerModule , MdNativeDateModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Injectable()
export class RegulationToolsService {

  public regulation: Object;

  constructor(public  _router: Router, public  authorize: AuthorizationService) {  }

  public printRegulation($regID): Promise<any> {
    var url;
    var method;
    var data;
    if ($regID instanceof Array) {
      url = `https://panel.cmcserv.com/panel/api/v1/user_regulation/print`;
      method = "Post";
      data = $regID
    } else {
      url = `https://panel.cmcserv.com/panel/api/v1/regulation/getText/${$regID}`;
      method = "Get";
      data = null;
    }
    return this.authorize.sendTokenizedRequest(data, url, method).then(response => {
      return response;
    }).catch(reject => {
      this._router.navigate(['profile', { 'outlets': { 'view': ['personal'] } }]).then(response => {
      });
    });
  }


  getRelated(regulationID) {
    const url = 'https://panel.cmcserv.com/panel/api/v1/regulation/related/';
    return this.authorize.sendTokenizedRequest(null , url + regulationID , 'GET');
  }
  public extractData(res) {
    let body = res.json().data;
    body.map(e=>{
     e.keywords = e.keywords[0];
     return e;
    });
    return body || {};
  }
}
