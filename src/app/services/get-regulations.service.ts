import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CookiesService } from './cookies.service';
import { Http } from '@angular/http';
import { Headers, Response, RequestOptions, RequestMethod, URLSearchParams, Request } from '@angular/http';
import 'rxjs';

@Injectable()
export class GetRegulationsService {

  constructor(public  http: Http, public  cookies: CookiesService) { }
  public regulations: any = [];
  public keywords = [];
  getRegulations($data) {
    function createRegulation($regulation, $lang) {
      const regulation: any = {};
      regulation['id']           = $regulation.id                 ? $regulation.id           : null;
      regulation['point']        = $regulation.point              ? $regulation.point        : null;
      regulation['question_id']  = $regulation.question_id        ? $regulation.question_id  : null;
      regulation['year']         = $regulation.year               ? $regulation.year         : null;
      regulation['number']       = $regulation.number             ? $regulation.number       : null;
      regulation['a_number']     = $regulation.a_number           ? $regulation.a_number     : null;
      regulation['type']       = $regulation.type                 ? $regulation.type       : null;
      regulation['state']        = $regulation.state              ? $regulation.state        : null;
      // marge en & ar keyword "in this case the there is only one keyword as current requrments"
      regulation['keywords']     = $regulation.keywords[0]     ?
      ($regulation.keywords[0] ? $regulation.keywords[0] : null) : null;
      regulation['name']         = $regulation['name'] ? $regulation['name'] : null;
      regulation['penalty']      = $regulation.penalty;
      return regulation;
    }
    if ($data) {
      if ( $data['_body'] === 'not found') {
        return [];
      }
      const regulations: any = JSON.parse($data['_body']),
          that = this;
          // tslint:disable-next-line:forin
      for (const key in regulations) {
        if(regulations[key] instanceof Object){
          if ( regulations[key]['name'] ) {that.keywords.push( regulations[key]['name'] ); }
          if ( regulations[key]['keywords'] !== null ){
          if ( regulations[key]['keywords'][0] ) { that.keywords.push( regulations[key]['keywords'][0] ); }
          }
          if (that.cookies.getCookie('lang') === 'en') {
            that.regulations.push(createRegulation(regulations[key], 'en'));
          } else if (that.cookies.getCookie('lang') === 'ar') {
            that.regulations.push(createRegulation(regulations[key], 'ar'));
          } else {
            that.cookies.setCookie('lang', 'en', 7);
            that.regulations.push(createRegulation(regulations[key], 'en'));
          }
        } 
        
      }
    }
    return this.regulations;
  }

}
