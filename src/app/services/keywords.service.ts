import { Injectable } from '@angular/core';
import { CookiesService } from './cookies.service';
import { Http } from '@angular/http';
import { Headers, Response, RequestOptions, RequestMethod, URLSearchParams, Request } from '@angular/http';
import 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class KeywordsService {

  public keywords_lib: any ;
  public keywordsUrl = 'https://panel.cmcserv.com/panel/api/v1/keyword/getAllText';

  constructor( public  http: Http, public  cookies: CookiesService,  public router: Router) {
    if (this.cookies.getCookie('api_token')) {
    // GET KEYWORDS REQUEST
      this.getKeywords();
    }
  }


  getKeywords(){
    this.sendTokenizedRequest(null, this.keywordsUrl, 'Get' ).then(response => {
      this.keywords_lib = JSON.parse(response['_body']);
      console.log(this.keywords_lib);
    }).catch(reject => {
      console.log(reject);
    });
  }


  sendTokenizedRequest($data: any, $url: string, $method: string, $tokenName: string = 'api_token' ): Promise<any> {
    const $tokenValue = this.cookies.getCookie($tokenName),
        headerObj   = { 'Content-Type' : 'application/json'};
    headerObj[$tokenName] = $tokenValue;
    const headers: Headers = new Headers(headerObj),
        submitedData: any = $data,
        requestOptions: RequestOptions = new RequestOptions({
          method: $method,
          url: $url,
          headers: headers,
          body: submitedData
        });
    return this.http.request(new Request(requestOptions)).toPromise()
      .then(response => {
        return response;
      })
      .catch(reject => {
        if ( reject.status === 401 ) {
          this.cookies.setCookie('api_token', null, 0);
          this.router.navigate(['/']);
        }
        return reject;
      });
  }

}
