import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './authentication.service';
import { Http } from '@angular/http';
import { Headers, Response, RequestOptions, RequestMethod, URLSearchParams, Request } from '@angular/http';
import 'rxjs';

@Injectable()
export class TextService {

  constructor(public  http: Http, public auth: AuthenticationService) { }

    getFirstSecText() {
        return this.getPage('100');
    }
    getAboutusSecText() {
        return this.getPage('99');
    }
    getServices() {
        return this.getPages();
    }

    getPage($pageId): Promise<any> {
        const url = 'https://panel.cmcserv.com/panel/api/v1/page/getById/' + $pageId;
        return this.auth.get(url).then(response => {
          return response['data'];
        }).catch(reject => {
          return reject;
        });
      }

      getPages(): Promise<any> {
        const url = 'https://panel.cmcserv.com/panel/api/v1/page/9';
        return this.auth.get(url).then(response => {
          return response['data'];
        }).catch(reject => {
          return reject;
        });
      }

}
