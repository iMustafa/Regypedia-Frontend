import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { CookiesService } from './cookies.service';
import { Http } from '@angular/http';
import { Headers, Response, RequestOptions, RequestMethod, URLSearchParams, Request } from '@angular/http';
import 'rxjs';

@Injectable()
export class AuthenticationService {

  constructor(private http: Http, private cookies: CookiesService, private router: Router, public dialog: MdDialog) { }
  /*
    $data   => Submited Form Data
    $url    => Form action ( Submits the data to a URL )
    $cookie => Sets a cookie of any name to remember the user authentication
    ( * )   => this._getToken => where this = response && _getToken is a method provided by the API
  */
  public submitData($data, $url, $cookie = null): Promise<any> {
    let headers: Headers = new Headers({ "Content-Type": "application/json" }),
        submitedData: Object = $data,
        requestOptions: RequestOptions = new RequestOptions({
          method: RequestMethod.Post,
          url: $url,
          headers: headers,
          body: JSON.stringify(submitedData)
        });
    return this.http.request(new Request(requestOptions)).toPromise()
      .then(response => {
        console.log($cookie , !this.cookies.checkCookie($cookie) , 'check conds');
        if ($cookie && !this.cookies.checkCookie($cookie)) {
          this.cookies.setCookie($cookie, this._getToken(response)[$cookie], 7);  // ( * )
          return true;
        } else {
          return false;
        }
      })
      .catch(reject => {
        return false;
      });
  }

  public get( $url): Promise<any> {
    let headers: Headers = new Headers({ "Content-Type": "application/json" }),
        requestOptions: RequestOptions = new RequestOptions({
          method: RequestMethod.Get,
          url: $url,
          headers: headers,
          body: null
        });
    return this.http.request(new Request(requestOptions)).toPromise()
      .then(response => {
        if (response.status === 400 ) {
          return false;
        }else {
          return JSON.parse(response['_body']);
        }
      })
      .catch(reject => {
        return false;
      });
  }

  public unauthorized($data, $url, $cookie = null): Promise<any> {
    const headers: Headers = new Headers({ "Content-Type": "application/json" }),
        submitedData: Object = $data,
        requestOptions: RequestOptions = new RequestOptions({
          method: RequestMethod.Post,
          url: $url,
          headers: headers,
          body: JSON.stringify(submitedData)
        });
    return this.http.request(new Request(requestOptions)).toPromise()
      .then(response => {
        if (response.status === 200 ) {
          return true;
        }else {
          alert(response['_body']);                          
          return false;
        }
      })
      .catch(reject => {
        alert(reject['_body']);                
        return false;
      });
  }
  

  public isLoggedIn(): Boolean {
    if (this.cookies.checkCookie('api_token')) {
      return true;
    } else {
      return false;
    }
  }

  private _getToken($data: Response) {
    let body = JSON.parse($data['_body']);
    return body.data || null;
  }
}
