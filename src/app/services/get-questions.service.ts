import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CookiesService } from './cookies.service';
import { AuthorizationService } from './authorization.service';
import { Http } from '@angular/http';
import { Headers, Response, RequestOptions, RequestMethod, URLSearchParams, Request } from '@angular/http';
import 'rxjs';

@Injectable()
export class GetQuestionsService {

  constructor(public  authorize: AuthorizationService, public  cookies: CookiesService, public http: Http) { }
  public questions: any = [];

  getQuestions(): Promise<any> {
    const url = 'https://panel.cmcserv.com/panel/api/v1/question',
      method = 'Get',
      token = 'api_token';
    return this.authorize.sendTokenizedRequest(null, url, method, token)
      .then(response => {
       return this.getUserAnsweredQuestions(response);
      })
      .catch(reject => {
        return reject;
      });
  }

  getUserAnsweredQuestions($questions: any): Promise<any> {
    const that = this;

    function createQuestion($question: any, $answer: any = null) {
      $question['answer'] = $answer;
      if ( $question['text'] !== null){
        if ( $question['help'] && $question['help']  ) {

          $question['help']['ar'] = $question['help']['ar'].split( '$' );
          $question['help']['en'] = $question['help']['en'].split( '$' );
          }
          return $question;
      }
        return null;
    }

    const allQuestionsParsedResponse = JSON.parse($questions['_body']),
      url = 'https://panel.cmcserv.com/panel/api/v1/user_question/getByUser',
      method = 'Get',
      token = 'api_token';
    return this.authorize.sendTokenizedRequest(null, url, method, token)
      .then(response => {
        const answerdQuestionsResponse = JSON.parse(response['_body']);
        allQuestionsParsedResponse.forEach(function (allQuestionsResponseObject, index) {
          if (allQuestionsResponseObject.id in answerdQuestionsResponse) {
            that.questions.push(createQuestion(allQuestionsResponseObject, answerdQuestionsResponse[allQuestionsResponseObject.id]));
          } else {
            that.questions.push(createQuestion(allQuestionsResponseObject));
          }
        });
        return this.questions.filter( item => { return (item == null) ?  false : true;  });
      })
      .catch(reject => {
        return reject;
      });
  }

  submitNewAnswer($questionID, $answer): Promise<any> {
    const data   = {'question_id' : $questionID, 'answer' : $answer},
          url    = 'https://panel.cmcserv.com/panel/api/v1/user_question/updateByQuestionId',
          method = 'Post',
          token  = 'api_token';
    return this.authorize.sendTokenizedRequest( data, url, method, token)
    .then(response => {
      return response;
    })
    .catch(reject => {
      return reject;
    });
  }

}
