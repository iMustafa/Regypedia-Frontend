import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CookiesService } from './cookies.service';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Headers, Response, RequestOptions, RequestMethod, URLSearchParams, Request } from '@angular/http';
import 'rxjs';

@Injectable()
export class AuthorizationService {

  constructor(public  http: Http, public  cookies: CookiesService ,  public router: Router) { }
  
  /*
    $data       => Submited Form/Request Data
    $url        => Form action ( Submits the data to a URL )
    $method     => Submited Form/Request Method
    $tokenName  => the api token with a deffult values 
  */
  sendTokenizedRequest($data: any, $url: string, $method: string, $tokenName: string = 'api_token'  ): Promise<any> {
    const $tokenValue = this.cookies.getCookie($tokenName),
        headerObj   = {'Content-Type' : 'application/json'};
    headerObj[$tokenName] = $tokenValue;
    headerObj['req_date'] = new Date().getDate()+'-'+ (new Date().getMonth()+1)+'-'+new Date().getFullYear();
    const headers: Headers = new Headers(headerObj),
        submitedData: any = $data,
        requestOptions: RequestOptions = new RequestOptions({
          method: $method,
          url: $url,
          headers: headers,
          body: submitedData
        });
    return this.http.request(new Request(requestOptions)).toPromise()
      .then(response => {
        //console.log(response , 'RES');
        return response;
      })
      .catch(reject => {
        if ( reject.status === 401 ) {
          this.cookies.setCookie('api_token', null, 0);
          this.router.navigate(['/']);
          }
        return reject;
      });
  }
}
