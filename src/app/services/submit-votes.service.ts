import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthorizationService } from './authorization.service';
import { Http } from '@angular/http';
import { Headers, Response, RequestOptions, RequestMethod, URLSearchParams, Request } from '@angular/http';
import 'rxjs';

@Injectable()
export class SubmitVotesService {

  constructor(public  http: Http, public authorize: AuthorizationService) { }

  submitVote($voteID, $answer): Promise<any> {
    var data: any = {vote_id: $voteID ,choice : JSON.stringify($answer)},
        url: string = "https://panel.cmcserv.com/panel/api/v1/user_vote/add",
        method: string = "Post";
    console.log(data);
    return this.authorize.sendTokenizedRequest(data, url, method).then(response => {
      return response;
    }).catch(reject => {
      return reject;
    });
  }

}
