import { Component , AfterViewInit } from '@angular/core';
import { TranslationService } from './services/translation.service';
import { CookiesService } from './services/cookies.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TranslationService, CookiesService]
})
export class AppComponent implements AfterViewInit{
  constructor(public  translation: TranslationService, public  cookiesService: CookiesService) {
    if (this.cookiesService.checkCookie('lang')) {
      // var lang = this.cookiesService.getCookie('lang');
    } else {
      this.cookiesService.setCookie('lang', 'en', 14);
    }
     this.translation.innerWidth = window.innerWidth;
  }
  ngAfterViewInit() {
    this.translation.innerWidth = window.innerWidth;
    if (this.cookiesService.checkCookie('background')) {
    document.getElementById('body').style.backgroundColor = this.cookiesService.getCookie('background');
    }else {
    document.getElementById('body').style.backgroundColor = '#efefef';
    }
  }
  onResize(event) {
    // tslint:disable-next-line:radix
    this.translation.innerWidth = parseInt( event.target.innerWidth );
    
  }
}
