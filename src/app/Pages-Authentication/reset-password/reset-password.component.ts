import { Component, OnInit } from '@angular/core';
import { Router, RouterModule , ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CookiesService } from '../../services/cookies.service';
import { TranslationService } from '../../services/translation.service';
// import { MdDialog } from '@angular/material';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {

  public  forgetPasswordForm: FormGroup;
  public  resetPasswordForm: FormGroup;
  public  lang: any = {};
  public  loginErr = false;
  public  sending = false;
  public  emailsent = false;
  public  value = '';

  constructor( public  router: Router, public  fb: FormBuilder,
     public  cookies: CookiesService, public langService: TranslationService ,
      public laraflatAuth: AuthenticationService , public arouter: ActivatedRoute) {
    if (this.cookies.getCookie('lang') === 'en') {
       this.lang = this.langService.en;
    }else if (this.cookies.getCookie('lang') === 'ar') {
      this.lang = this.langService.ar;
    }else { this.lang = this.langService.en; }
    this.resetPasswordForm = fb.group({
      'token': [null, Validators.compose([ Validators.required])],
      'new_password': [null, Validators.compose([ Validators.required])]
    });
    this.forgetPasswordForm = fb.group({
      'email': [null, Validators.compose([Validators.email, Validators.required])]
    });
  }

  ngOnInit() {
    this.arouter.queryParams.subscribe( params => {
      if (params['key']) {
        this.value = params['key'];
        this.resetPasswordForm.controls['token'].setValue(this.value);
        this.emailsent = true;
      }
    });
  }

  submitReset($formData) {
    this.sending = true;
    const url     = 'https://panel.cmcserv.com/panel/api/v1/users/updatePassword',
        cookie  = 'api_token',
        that = this;
    this.laraflatAuth.unauthorized($formData, url, cookie)
    .then(response => {
      that.sending = false;
      if (response) {
        this.router.navigate(['/']);
      } else {
        that.loginErr = true;
      }
    })
    .catch(reject => {
      that.sending = false;
      return false;
    });
  }

  submitForget($formData) {
    this.sending = true;
    const url     = 'https://panel.cmcserv.com/panel/api/v1/users/requestToUpdatePassword',
        cookie  = 'api_token',
        that = this;
    this.laraflatAuth.unauthorized($formData, url, cookie)
    .then(response => {
      that.sending = false;
      if ( response ) {
        this.emailsent = true;
        that.loginErr = false;
      }else {
        that.loginErr = true;
      }
    })
    .catch(reject => {
      that.sending = false;
      return false;
    });
  }

}
