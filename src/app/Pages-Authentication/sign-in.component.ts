import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslationService } from '../services/translation.service';
import { CookiesService } from '../services/cookies.service';
import { AuthenticationService } from '../services/authentication.service';
import { Observable } from 'rxjs/Observable';
import {MdChipsModule} from '@angular/material';
import { ForgetPasswordComponent } from './forget-password.component';
import { Router } from '@angular/router';
import { log } from 'util';

@Component({
  selector: 'login-dialog',
  template: `
    <md-icon  md-icon-button (click)="closeDialog()" color="warn">close</md-icon>
    <form [formGroup]='signinForm' (ngSubmit)='submitSigninForm(signinForm.value)'>
      <h2 style='text-align: center'>{{(lang)?.userLogin}}</h2>
      <md-input-container class='col-md-12 row' style="min-width: 100%;">
        <input mdInput type='text' class="col-md-12" [placeholder]="(lang)?.email" formControlName='email'>
      </md-input-container>
      <md-input-container class='col-md-12 row' style="min-width: 100%;">
        <input mdInput type='password' class="col-md-12" [placeholder]="(lang)?.password" formControlName='password'>
      </md-input-container>

      <div class='col-md-12 row' *ngIf="errlog.length > 1">
      <div class="alert alert-danger col-md-12 center-block" role="alert"> {{errlog}}</div>
      </div>

      <a style="color:blue; cursor:pointer;"  routerLink="/resetpassword" (click)="closeDialog()" >{{(lang)?.forgetPassword}}</a>
      <div style='text-align:center'>
      <button [disabled]="sending" md-raised-button  type='submit' color="accent" class='center-block' >
        <md-spinner *ngIf='sending' class="center-block"></md-spinner> <span *ngIf='!sending' > {{(lang)?.userLogin}} </span>
      </button>
      </div>
    </form>
  `,
  styles: [ 'md-chip {max-width: 100%;}' , 'md-spinner{max-height:36px; display:inline-block;}' , 'md-icon{cursor : pointer;}' ],
  providers: [TranslationService, AuthenticationService]
})
export class SigninComponent {

  public  signinForm: FormGroup;
  public  lang: any = {};
  public  errlog = '';
  public  sending = false;

  constructor(public dialog: MdDialog, public  fb: FormBuilder,
  public  cookies: CookiesService, public langService: TranslationService,
  public laraflatAuth: AuthenticationService , private router: Router) {

    if (this.cookies.getCookie("lang") === "en") { this.lang = this.langService.en; }
    else if (this.cookies.getCookie("lang") === "ar") { this.lang = this.langService.ar; }
    else { this.lang = this.langService.en; }
    this.signinForm = fb.group({
      "email": [null, Validators.compose([Validators.email, Validators.required])],
      "password": [null, Validators.compose([Validators.pattern(/^(?=.*[a-z])/), Validators.required])]
    });
  }

  closeDialog() { this.dialog.closeAll(); }

  // old forget password dialog
  // forgetPassword() {
  //   this.closeDialog();
  //   this.dialog.open(ForgetPasswordComponent);
  // }

  submitSigninForm($formData) {
    this.sending = true;
    const url     = "https://panel.cmcserv.com/panel/api/v1/users/login",
        cookie  = "api_token";
    this.laraflatAuth.submitData($formData, url, cookie)
    .then(response => {

      this.sending = false;
      if (response === true ) {
        this.router.navigate(['/user']);
        this.closeDialog();
      } else if ( response === false ) {
        console.log('====================================');
        console.log('FALSE');
        console.log('====================================');
        if (this.cookies.getCookie("lang") === "en") { this.errlog = 'Error unknown email or password.';
        }else {
           this.errlog = 'خطأ أثناء تسجيل الدخول ، يرجى التأكد من اسم المستخدم و كلمة المرور ';
        }
      }

    })
    .catch(reject => {
      this.sending = false;
      return false;
    });
  }

    

}
