import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { TranslationService } from '../services/translation.service';
import { CookiesService } from '../services/cookies.service';
import { AuthenticationService } from '../services/authentication.service';
import { Observable } from 'rxjs/Observable';
// import { MdTabsModule, MdCardModule, MdButton, MdInputModule } from '@angular/material';

@Component({
  selector: 'app-sign-up',
  template: `
  <button md-icon-button (click)="closeDialog()" color="warn"> <md-icon>close</md-icon> </button>  
    <h3>{{(lang)?.currentLang.demoReq}}</h3>
    <form *ngIf="!sent" class='ng-form' [formGroup]='signupForm' (ngSubmit)='signup(signupForm.value)'>
      <md-input-container >
        <input mdInput type='name' [style.text-align]='(lang)?.textAlignValue'
        [placeholder]="(lang)?.currentLang.name+ ' *'" formControlName='name'>
      </md-input-container>
      <md-input-container >
        <input mdInput type='email' [placeholder]="(lang)?.currentLang.email+ ' *'" formControlName='email'>
      </md-input-container>
      <md-input-container >
        <input mdInput type='text' [style.text-align]='(lang)?.textAlignValue'
         [placeholder]="(lang)?.currentLang.companyName+ ' *'" formControlName='company'>
      </md-input-container>
      <md-input-container >
        <input mdInput type='text' [style.text-align]='(lang)?.textAlignValue'
         [placeholder]="(lang).currentLang.phoneNumber+ ' *'" formControlName='phone'>
      </md-input-container>
      <md-input-container >
      <input mdInput type='note' [style.text-align]='(lang)?.textAlignValue'
      [placeholder]="(lang)?.currentLang.note" formControlName='note'>
    </md-input-container>
        <div style='text-align:center'>
          <button md-raised-button type='submit' class='col-xs-6 col-xs-push-3' [disabled]="sending || !signupForm.valid">
            <span *ngIf="!sending"> {{lang.send}} </span> <span *ngIf="sending"> {{lang.sending}} </span>
          </button>
        </div>
      <div class='clearfix'></div>
    </form>
    <div style="text-align:center;" *ngIf="sent">
      <md-icon style="color:green; font-size:40px;">beenhere</md-icon>
      <h4>{{lang.sent}}</h4>
    </div>

  `,
  styles: [
    'md-input-container{ width: 100%; } form{max-width:350px;}'
  ],
  providers: [AuthenticationService]
})
export class SignUpComponent implements OnInit {

  public signupForm: FormGroup;
  public sending = false;
  public sent = false;
  public lang; 
  constructor(public dialog: MdDialog, public fb: FormBuilder,
      public laraflatAuth: AuthenticationService , public  cookies: CookiesService ) {
        
        this.signupForm = fb.group({
      'name': [null, Validators.required],
      'email': [null, Validators.compose([Validators.email, Validators.required])],
      'company': [null, Validators.required],
      'phone': [null, Validators.compose([Validators.pattern(/[0-9+]/), Validators.required])],
      'note': [null],
      'submit': [null]
    });
  }

  signup($formData) {
    this.sending = true;
    if ($formData && true) {
      const url = 'https://panel.cmcserv.com/panel/api/v1/users/requestDemo';
      this.laraflatAuth.unauthorized($formData, url).then( response => {
        console.log(response);
        if (response) {
        this.sent = true;
          setTimeout(function() {
            this.dialog.close();
          }, 500);
        return true;
        }else {
          this.sending = false;
        }
      }).catch( err => {
      });
    } else {
      return false;
    }
  }
  closeDialog() { this.dialog.closeAll(); }
  ngOnInit() { 
    this.lang = new TranslationService(this.cookies);
  }
}
