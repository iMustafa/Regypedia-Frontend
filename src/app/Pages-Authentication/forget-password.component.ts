import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CookiesService } from '../services/cookies.service';
import { TranslationService } from '../services/translation.service';
import { MdDialog } from '@angular/material';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-forget-password',
  template: `
  <button md-icon-button (click)="closeDialog()" color="warn"> <md-icon>close</md-icon> </button>
  <form [formGroup]='forgetPasswordForm' (ngSubmit)='submitForgetPasswordComponent(forgetPasswordForm.value)'>
        <h2 style='text-align: center'>{{(lang)?.resetPassword}}</h2>
        <md-input-container class='col-md-12' style='width:100%'>
          <input mdInput type='email' [placeholder]="(lang)?.email" formControlName='email'>
        </md-input-container>
        <div class='clearfix'></div>
        <div *ngIf="loginErr" style='text-align:center'>
        <span style="color:red; text-align:center;"> {{lang.noEmail}} </span> 
       </div>
        <div style='text-align:center'>
        <button md-raised-button type='submit' class='col-xs-6 col-xs-push-3' [disabled]="sending">{{(lang)?.reset}}</button>
        </div>
        <div class='clearfix'></div>
      </form>

     
  `,
})
export class ForgetPasswordComponent implements OnInit {

  public  forgetPasswordForm: FormGroup;
  public  lang: any = {};
  public  loginErr = false;
  public  sending = false;

  constructor( public dialog: MdDialog , public  router: Router, public  fb: FormBuilder,
     public  cookies: CookiesService, public langService: TranslationService , public laraflatAuth: AuthenticationService) {
    if (this.cookies.getCookie('lang') === 'en') {
       this.lang = this.langService.en;
    }else if (this.cookies.getCookie('lang') === 'ar') {
      this.lang = this.langService.ar;
    }else { this.lang = this.langService.en; }
    this.forgetPasswordForm = fb.group({
      'email': [null, Validators.compose([Validators.email, Validators.required])]
    });
  }

  ngOnInit() {

  }
  closeDialog() { this.dialog.closeAll(); }

  submitForgetPasswordComponent($formData) {
    this.sending = true;
    const url     = 'https://panel.cmcserv.com/panel/api/v1/users/requestToUpdatePassword',
        cookie  = 'api_token',
        that = this;
    this.laraflatAuth.unauthorized($formData, url, cookie)
    .then(response => {
      that.sending = false;
      if ( ! response ) {
        that.loginErr = true;
      }else {
        this.closeDialog();
        this.router.navigate(['/resetpassword']);
      }
    })
    .catch(reject => {
      that.sending = false;
      return false;
    });
  }
}
