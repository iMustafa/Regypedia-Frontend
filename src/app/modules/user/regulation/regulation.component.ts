import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { AuthorizationService } from '../../../services/authorization.service';
import { TranslationService } from '../../../services/translation.service';
import { RegulationToolsService } from '../../../services/regulation-tools.service';
import { MdDatepickerModule, MdNativeDateModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { RegulationData, RegulationDataSource, RegulationsDB } from './classes';

@Component({
  selector: 'app-regulation',
  templateUrl: './regulation.component.html',
  styleUrls: ['./regulation.component.css'],
  providers: [MdDialog, AuthorizationService, RegulationToolsService]
})
export class RegulationComponent implements OnInit {

  @ViewChild('note') note: ElementRef;
  public noteTemp = '';
  public regulationID: string;
  public regulation: Object;
  public regulation404 = false;
  public purchased = false;
  public currState = '';
  public currDate = '';
  public currNote = '';
  public waiting = true;
  public saved = false;
  public saving = false;
  public url = 'https://panel.cmcserv.com/panel/public/'
  public minDate = new Date();
  displayedColumns = ['name', 'type', 'number', 'year', 'a_number', 'tools'];
  dataSource: RegulationDataSource | null;
  dataDB = new RegulationsDB();
  spinner = true;


  constructor(public translation: TranslationService, public _router: Router, public _route: ActivatedRoute
    , public authorize: AuthorizationService, public _dialog: MdDialog, public regulationTool: RegulationToolsService) {
  }

  public _purchaseRegulation() {
    console.log("tring to get the regulation ... ");
    var url: string = `https://panel.cmcserv.com/panel/api/v1/regulation/getText/${this.regulationID}`,
      method: string = "Get";
    this.authorize.sendTokenizedRequest(null, url, method).then(response => {
      var regulation: Object = JSON.parse(response._body).data;
      this.regulation = regulation;

      if (this.regulation['user_regulation']) {
        this.currState = this.regulation['user_regulation']['state'];
        this.currDate = this.regulation['user_regulation']['notifications'];
        this.currNote = this.regulation['user_regulation']['note'] ? this.regulation['user_regulation']['note'] : '';
      }else{
        this.currState = '';
        this.currState = '';
        this.currNote = '';
      }
      console.log(this.regulation);
    }).catch(reject => {
      this._router.navigate(['/user/404']);
    });
  }

  public saveNote() {
    this.waiting = false;
    this.saved = false;
    this.saving = true;
    this._updateAttr(this.currState, this.currDate, this.note.nativeElement.value)
  }

  public _updateAttr($state, $date, $note) {
    const RegID: string = this.regulationID;
    const data: Object = {
      regulation_id: RegID,
      state: $state,
      notifications: $date !== '' && $date !== '01-01-0001' ? $date : ($note !== '' && $note !== 'null'  ? new Date().getDate()+'-'+ (new Date().getMonth()+1)+'-'+new Date().getFullYear() : '') ,
      note: $note
    }
    const url = 'https://panel.cmcserv.com/panel/api/v1/user_regulation/updateAttributes'
    const method = 'Post'
    this.authorize.sendTokenizedRequest(data, url, method).then(response => {
      const state = JSON.parse(response._body).state
      const notifications = JSON.parse(response._body).notifications
      const note = JSON.parse(response._body).note
      this.waiting = false;
      this.saved = true;
      this.saving = false;
    }).catch(reject => {
      console.log(reject)
    });
  }

  public updateNotificationDate($date) {
    this.waiting = false;
    this.saved = false;
    this.saving = true;
    $date = this.convertDate($date);
    console.log($date);
    this.currDate = $date;
    this._updateAttr(this.currState, this.currDate, this.note.nativeElement.value)
  }

  public checkToSaveNote() {
    if (this.note) {
      if (this.noteTemp !== this.note.nativeElement.value) {
        this.noteTemp = this.note.nativeElement.value;
        this.saveNote();
      }
    }
  }

  ngOnInit() {
    const timer = Observable.timer(3000, 2200);
    timer.subscribe(() => this.checkToSaveNote());
    this._route.params.subscribe((params) => {
      this.spinner = true;
      this.regulation = null;
      this.regulationID = params['regulationID'];
      this._purchaseRegulation();
      this.regulationTool.getRelated(this.regulationID).then((res) => {
        res = this.regulationTool.extractData(res);
        console.log(res);
        this.dataSource = new RegulationDataSource(this.dataDB.setData(res));
        this.spinner = false;
      });
    });
  }

  convertDate(str) {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2)
    return [day, mnth, date.getFullYear()].join('-')
  }

}
