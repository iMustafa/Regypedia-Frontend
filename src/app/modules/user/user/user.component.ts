import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute ,NavigationEnd  } from '@angular/router';
import { MdCardModule } from '@angular/material';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [ AuthenticationService ]
})
export class UserComponent implements OnInit {

  constructor(public  _activatedRoute : ActivatedRoute, public  _router: Router, public  _auth: AuthenticationService) {
    if (!this._auth.isLoggedIn()) {
      this._router.navigate(['/']);
    }
  }

  ngOnInit() {
    this._router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0);
  });
  }

}
