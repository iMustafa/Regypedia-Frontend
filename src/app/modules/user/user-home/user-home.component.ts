import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../services/translation.service';
@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {

  constructor( public lang: TranslationService) { }

  ngOnInit() {
  }

}
