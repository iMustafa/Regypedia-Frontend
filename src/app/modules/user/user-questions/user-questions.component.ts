import { Component, OnInit, AfterViewInit, ElementRef, TemplateRef } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { AuthorizationService } from '../../../services/authorization.service';
import { GetQuestionsService } from '../../../services/get-questions.service';
import { TranslationService } from '../../../services/translation.service';

@Component({
  selector: 'app-user-questions',
  templateUrl: './user-questions.component.html',
  styleUrls: ['./user-questions.component.css'],
  providers: [AuthorizationService, GetQuestionsService]
})
export class UserQuestionsComponent implements AfterViewInit {



  public spinner = true;
  public loaded: Boolean = false;
  public questions: any = [];
  public progressBarConfig = {
    value: null,
    incrementValue: null,
    answeredIDs: []
  };



  constructor(public authorize: AuthorizationService, public getUserQuestions: GetQuestionsService,
    public translation: TranslationService, public dialog: MdDialog) {
  }


  public saving: Boolean = false;
  public saved: Boolean = false;
  public waiting: Boolean = true;
  public error: Boolean = false;

  submitNewValue($questionID, $answer) {
    this.waiting = false;
    this.saved = false;
    this.saving = true;
    this.getUserQuestions.submitNewAnswer($questionID, $answer)
      .then(response => {
        var that = this;
        this.saving = false;
        that.saved = true;
        this.progressBarDet(true, $questionID);
        return response;
      })
      .catch(reject => {
        var that = this;
        this.error = true;
        setTimeout((err) => {
          that.saving = false;
          that.error = false;
        }, 2000);
        return reject;
      });
  }

  public progressBarDet(newQuestion = false, $questionID = null) {
    var that = this;
    setTimeout(function () {
      that.progressBarConfig.incrementValue = 100 / parseInt(that.questions.length);
      var BreakException = {};
      try {
        that.questions.forEach((questionInfo, index) => {
          if (that.progressBarConfig.answeredIDs.indexOf($questionID) === -1 || $questionID === null) {
            if (newQuestion) {
              that.progressBarConfig.answeredIDs.push($questionID);
              that.progressBarConfig.value += that.progressBarConfig.incrementValue;
              throw BreakException;
            } else if ($questionID === null && that.questions[index].answer !== null) {
              that.progressBarConfig.answeredIDs.push(that.questions[index].id);
              that.progressBarConfig.value += that.progressBarConfig.incrementValue;
            }

          }
        });
      } catch (e) {
        if (e !== BreakException) {
          console.log('BREAK');
          throw e;
        }
      }

    }, 500);
  }

  ngAfterViewInit() {
    this.getUserQuestions.getQuestions()
      .then(response => {
        console.log(response);
        
        var that = this;
        if (response instanceof Array) {
          this.questions = response;
          this.loaded = !this.loaded;
          this.progressBarDet();
        }
      })
      .catch(reject => {
        console.log(reject);
      });
  }
}
