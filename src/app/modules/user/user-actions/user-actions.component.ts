import { Component, OnInit } from '@angular/core';
import { ActionDataSource , ActionsDB} from './classes';
import { ActionsService } from '../../../services/actions.service';
import { AuthorizationService } from '../../../services/authorization.service';
import { TranslationService } from '../../../services/translation.service';

@Component({
  selector: 'app-user-actions',
  templateUrl: './user-actions.component.html',
  styleUrls: ['./user-actions.component.css'],
  providers: [ ActionsService , AuthorizationService ]
})
export class UserActionsComponent implements OnInit {

  displayedColumns = ['regulation', 'action', 'date', 'open' ];
  dataSource: ActionDataSource | null ;
  dataDB = new ActionsDB();
  spinner = true;

  constructor( public actionService: ActionsService , public authorize: AuthorizationService , public translation: TranslationService ) {
  }

  ngOnInit() {

    this.actionService.getActions().then(response => {
      response = JSON.parse( response['_body']);
      this.dataSource =
      new ActionDataSource(this.dataDB.setData(response));
      this.spinner = false;
      return response;
    })
    .catch(reject => {
      return reject;
    });


}

}

