import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { MdPaginator, MdSort } from '@angular/material';


export interface RegulationData {
    id: string;
    name: string;
    number: number;
    a_number: number;
    point: number;
    year: number;
    type: string;
    text: string | null;
    state: string;
  }

  export interface ActionData {
    id: string;
    note: string;
    regulation: RegulationData;
    notifications: string;
  }

  export class ActionsDB {
    dataChange: BehaviorSubject<any> = new BehaviorSubject<any>([]);
    get data(): any { return this.dataChange.value; }


    setData($data) {
      const that = this;
      // tslint:disable-next-line:forin
      for ( var key in $data ) {
        const copiedData = that.data.slice();
        copiedData.push($data[key]);
        that.dataChange.next(copiedData);
        console.log($data[key]);
      }
      return this;
    }

}

  export class ActionDataSource extends DataSource<any> {
    _filterChange = new BehaviorSubject('');
    get filter(): string { return this._filterChange.value; }
    set filter(filter: string) { this._filterChange.next(filter); }

     public filteredData: ActionsDB[] = [];

    constructor(public  _exampleDatabase: ActionsDB = null) {
      super();
    }



    connect(): Observable<ActionsDB[]> {
   // list of all filter changes
    const displayDataChanges = [
        this._exampleDatabase.dataChange,
        this._filterChange,
    ];

      return Observable.merge(...displayDataChanges).map(() => {
        this.filteredData = this._exampleDatabase.data.slice().filter((item: ActionData) => {
          const searchStr = String(item.note + item.notifications + item.regulation.name).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });
        return this.filteredData;
      });
    }

    disconnect() {}

}
