import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { MdPaginator, MdSort } from '@angular/material';
import * as _ from 'lodash';


export interface RegulationData {
    id: string;
    name: string;
    number: number;
    a_number: number;
    point: number;
    year: number;
    type: string;
    text: string | null;
    state: string;
    keywords: any;
  }

  export class RegulationDB {
    dataChange: BehaviorSubject<any> = new BehaviorSubject<any>([]);
    get data(): any { return this.dataChange.value; }


    setData($data) {
      const that = this;
      $data.forEach(element => {
        const copiedData = that.data.slice();
        copiedData.push(element);
        that.dataChange.next(copiedData);
      });
      return this;
    }

}

  export class RegulationsDataSource extends DataSource<any> {
    _filterChange = new BehaviorSubject('');
    get filter(): string { return this._filterChange.value; }
    set filter(filter: string) { this._filterChange.next(filter); }

    _yearFilter = new BehaviorSubject('');
    get yearFilter(): string { return this._yearFilter.value; }
    set yearFilter(filter: string) { this._yearFilter.next(filter); }

    _numberFilter = new BehaviorSubject('');
    get numberFilter(): string { return this._numberFilter.value; }
    set numberFilter(filter: string) { this._numberFilter.next(filter); }


    _a_numberFilter = new BehaviorSubject('');
    get a_numberFilter(): string { return this._a_numberFilter.value; }
    set a_numberFilter(filter: string) { this._a_numberFilter.next(filter); }

    _viewChange = new BehaviorSubject('');
    get viewChange(): string { return this._viewChange.value; }
    set viewChange(filter: string) { this._viewChange.next(filter); }

    public allViewData: RegulationData[] = [];
    public filteredData: RegulationData[] = [];
    public dropdown_year: number[] = [];

    constructor(public  _exampleDatabase: RegulationDB) {
      super();
    }



    connect(): Observable<RegulationData[]> {
   // list of all filter changes
    const displayDataChanges = [
        this._exampleDatabase.dataChange,
        this._filterChange,
        this._yearFilter,
        this._numberFilter,
        this._a_numberFilter,
        this._viewChange
    ];

      return Observable.merge(...displayDataChanges).map(() => {
        this.allViewData = this._exampleDatabase.data.slice().filter((item: RegulationData) => {
          if (this.viewChange.toLowerCase() === 'library') {
            const searchStr = (String(item.state));
            // when user select the library reglations the table will show the library and the favorite too
            return (searchStr.indexOf(this.viewChange.toLowerCase())  !== -1) || (searchStr.indexOf('favorite')  !== -1) ;
          }else {
            const searchStr = (String(item.state));
            return searchStr.indexOf(this.viewChange.toLowerCase()) !== -1;
          }
        });
          const that = this;
          this.dropdown_year = [];
          this.allViewData.forEach(element => {
          that.dropdown_year.push(element.year);
          that.dropdown_year = _.uniq(that.dropdown_year);
         }

       );
        this.filteredData = this._exampleDatabase.data.slice().filter((item: RegulationData) => {
          const searchStr = String(item.name + item.type + item.keywords ? item.keywords.en+item.keywords.ar : '' ).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        }).filter((item: RegulationData) => {
          const searchStr = (String(item.year));
          return searchStr.indexOf(String(this.yearFilter).toLowerCase()) !== -1;
        }).filter((item: RegulationData) => {
            const searchStr = (String(item.number));
            return searchStr.indexOf(this.numberFilter.toLowerCase()) !== -1;
          }).filter((item: RegulationData) => {
            const searchStr = (String(item.a_number['ar'] + item.a_number['en']));
            return searchStr.indexOf(this.a_numberFilter.toLowerCase()) !== -1;
          }).filter((item: RegulationData) => {
            if (this.viewChange.toLowerCase() === 'library') {
              const searchStr = (String(item.state));
              // when user select the library reglations the table will show the library and the favorite too
              return (searchStr.indexOf(this.viewChange.toLowerCase())  !== -1) || (searchStr.indexOf('favorite')  !== -1) ;
            }else {
              const searchStr = (String(item.state));
              return searchStr.indexOf(this.viewChange.toLowerCase()) !== -1;
            }
          });

        return this.filteredData;
      });
    }

    disconnect() {}

}

  /** Constants used to fill up our data base. */
  const COLORS = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
    'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
  const NAMES = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
    'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
    'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];

    export interface UserData {
    id: string;
    name: string;
    progress: string;
    color: string;
  }

  /** An example database that the data source uses to retrieve data for the table. */
  export class ExampleDatabase {
    /** Stream that emits whenever the data has been modified. */
    dataChange: BehaviorSubject<any> = new BehaviorSubject<any>([]);
    get data(): any { return this.dataChange.value; }

    constructor() {
      // Fill up the database with 100 users.
      for (let i = 0; i < 100; i++) { this.addUser(); }
      console.log(this.dataChange, 'datachange');
    }

    /** Adds a new user to the database. */
    addUser() {
      const copiedData = this.data.slice();
      copiedData.push(this.createNewUser());
      this.dataChange.next(copiedData);
    }

    /** Builds and returns a new User. */
    public  createNewUser() {
      const name =
        NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
        NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

      return {
        id: (this.data.length + 1).toString(),
        name: name,
        progress: Math.round(Math.random() * 100).toString(),
        color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
      };
    }
  }

  /**
   * Data source to provide what data should be rendered in the table. Note that the data source
   * can retrieve its data in any way. In this case, the data source is provided a reference
   * to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
   * the underlying data. Instead, it only needs to take the data and send the table exactly what
   * should be rendered.
   */
  export class ExampleDataSource extends DataSource<any> {
    _filterChange = new BehaviorSubject('');
    get filter(): string { return this._filterChange.value; }
    set filter(filter: string) { this._filterChange.next(filter); }

    filteredData: UserData[] = [];
    renderedData: UserData[] = [];

    constructor(public  _exampleDatabase: ExampleDatabase, public  _sort: MdSort, public  _paginator: MdPaginator) {
      super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<UserData[]> {
      // Listen for any changes in the base data, sorting, filtering, or pagination
      const displayDataChanges = [
        this._exampleDatabase.dataChange,
        this._sort.mdSortChange,
        this._filterChange,
        this._paginator.page
      ];

      return Observable.merge(...displayDataChanges).map(() => {
        // Filter data
        this.filteredData = this._exampleDatabase.data.slice().filter((item: UserData) => {
          const searchStr = (item.name + item.color).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        console.log(this.renderedData , 'D');
        return this.renderedData;
      });
    }
    disconnect() { }
    sortData(data: UserData[]): any {
      if (!this._sort.active || this._sort.direction === '') { return data; }
      return data.sort((a, b) => {
        let propertyA: number | string = '';
        let propertyB: number | string = '';
        switch (this._sort.active) {
          case 'userId': [propertyA, propertyB] = [a.id, b.id]; break;
          case 'userName': [propertyA, propertyB] = [a.name, b.name]; break;
          case 'progress': [propertyA, propertyB] = [a.progress, b.progress]; break;
          case 'color': [propertyA, propertyB] = [a.color, b.color]; break;
          case 'name': break;
        }
        const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
        const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
        return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
      });
    }

  }
