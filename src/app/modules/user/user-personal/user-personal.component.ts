import { Component, AfterViewInit, ViewChild, ElementRef, Input } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthorizationService } from '../../../services/authorization.service';
import { CookiesService } from '../../../services/cookies.service';
import { GetRegulationsService } from '../../../services/get-regulations.service';
import { TranslationService } from '../../../services/translation.service';
import { TableFilteringService } from '../../../services/table-filtering.service';
import { RegulationToolsService } from '../../../services/regulation-tools.service';
import { RegulationsDataSource, RegulationDB } from './classes';
import { FormControl, NgModel } from '@angular/forms';

import { PreparePrintComponent } from '../prepare-print/prepare-print.component';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';10
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import * as _ from 'lodash';

@Component({
  selector: 'app-user-personal',
  templateUrl: './user-personal.component.html',
  styleUrls: ['./user-personal.component.css'],
  animations: [
    trigger('showFilters', [
      state('hidden', style({
        'height': 0
      })),
      state('shown', style({
        'max-height': '200px'
      })),
      transition('hidden <=> shown', animate('300ms ease-in-out'))
    ]),
    trigger('showSideNav', [
      state('hidden', style({
        'width': '0'
      })),
      state('shown', style({
        'width': '40%'
      })),
      transition('hidden <=> shown', animate('300ms ease-in-out'))
    ])
  ],
  providers: [
    AuthorizationService,
    GetRegulationsService,
    TableFilteringService,
    RegulationToolsService,
    PreparePrintComponent,
    { provide: 'window', useValue: window }
  ]
})

export class UserPersonalComponent implements AfterViewInit {

  @ViewChild('filter') filter: ElementRef;
  @ViewChild('yearFilter') yearFilter: ElementRef;
  @ViewChild('numberFilter') numberFilter: ElementRef;
  @ViewChild('aNumberFilter') aNumberFilter: ElementRef;
  displayedColumns = [ 'check','keyword' , 'type', 'number', 'year', 'a_number', 'id'];
  public spinner = true;
  public noResults = false;
  public nothingToShow = false;

  public regulations: any = [];
  public  _filtersAnimationState = 'hidden';
  public  _sideNavAnimationState = 'hidden';
  public  _view = 'library';

  public stateCtrl: FormControl;
  public filteredStates: Observable<any[]>;

  public selectedRegulationsObj: any = {};
  public selectedRegulationsArr: any = [];
  public checkBoxs: any = [];
  public selected= 0;
  public allSelected = false;
  regulationsDB = new RegulationDB();
  regulationsDataSource: RegulationsDataSource | null;

  constructor(public  authorize: AuthorizationService,
              public  cookies: CookiesService,
              public  getUserRegulations: GetRegulationsService,
              public  translation: TranslationService,
              public  filterTable: TableFilteringService,
              public  regullationTools: RegulationToolsService,
              public  printServ: PreparePrintComponent ) {
              // init the auto complete
                this.stateCtrl = new FormControl();
              }

  //  auto complete filter func
  public  filterStates(name: string) {
    if ( name !== '') {
    return this.getUserRegulations.keywords.filter(state =>
      state[this.translation.langKey].toLowerCase().indexOf(name.toLowerCase()) !== -1 );
    }else {
      return null;
    }
  }

  public changeView($value) {
    if (!this.regulationsDataSource) { return; }
    this.regulationsDataSource.viewChange = $value;
    this._view = $value;
    
    this.regulationsDataSource.yearFilter = this.regulationsDataSource.yearFilter in this.regulationsDataSource.dropdown_year ?
    this.regulationsDataSource.yearFilter : '';
    // console.log('Changed!');
    return $value;
  }

  public selectRegulation($reg, $this) {
    this.checkBoxs.push($this);
    if (this.selectedRegulationsObj[$reg.id]) {
      delete this.selectedRegulationsObj[$reg.id];
      this.selected --;
    } else {
      this.selectedRegulationsObj[$reg.id] = $reg;
      this.selected ++;
    }
  }

  public print($regID) {
    open(`http://localhost:4200/user/print/${$regID}`)
  }

  public printMultiRegulation() {
    var that = this
    this.selectedRegulationsArr = []
    this.allSelected = false
    this.checkBoxs.forEach(element => {
      element.checked = false
    })
    this.checkBoxs = []
    for (var key in this.selectedRegulationsObj) {
      this.selectedRegulationsArr.push(key)
    }
    this.selectedRegulationsObj = {}
    var uniqueIDArray = _.uniq(this.selectedRegulationsArr)

    open(`http://localhost:4200/user/print/${uniqueIDArray}`)

  }

  public  _updateAttr($reg, $state) {
    var data: Object = {
      regulation_id: $reg.id,
      state: $state
    };
    var url: string = "https://panel.cmcserv.com/panel/api/v1/user_regulation/updateAttributes";
    var method: string = "Post";
    this.authorize.sendTokenizedRequest(data, url, method).then(response => {
      $reg.state = $state;
      this.changeView(this._view);
    }).catch(reject => {
      console.log(reject);
    });
  }

  public _updateMultiAttr($state) {
    this.selectedRegulationsArr = [];
    this.allSelected = false;
    this.selected = 0;
    this.checkBoxs.forEach(element => {
      element.checked = false;
    });
    this.checkBoxs = [];
    // tslint:disable-next-line:forin
    for (var key in this.selectedRegulationsObj) {
      this.selectedRegulationsArr.push(key);
      this.selectedRegulationsObj[key].state = $state;
    }
    this.changeView(this._view);
    this.selectedRegulationsObj = {};
    var uniqueIDArray = _.uniq(this.selectedRegulationsArr);
    uniqueIDArray.forEach((id , index) => {
      var data: Object = {
        regulation_id: id,
        state: $state
      };
      var url: string = "https://panel.cmcserv.com/panel/api/v1/user_regulation/updateAttributes";
      var method: string = "Post";
      this.authorize.sendTokenizedRequest(data, url, method).then(response => {
        console.log('UPDATED!');
      }).catch(reject => {
        console.log(reject);
      });
    });
  }

  public _showRow($reg) {
    console.log($reg);
    return true;
  }
  checkIsNoResults() {
    return this.regulationsDataSource && this.regulationsDataSource.filteredData.length === 0
    && this.regulationsDataSource._exampleDatabase.data.length > 0;
   }
   checkIfNothingToShow() {
     return this.regulationsDataSource && this.regulationsDataSource._exampleDatabase.data.length === 0;
   }
  ngAfterViewInit() {
    const url = 'https://panel.cmcserv.com/panel/api/v1/user_regulation/getByUser',
      method = 'GET',
      token = 'api_token';
    this.authorize.sendTokenizedRequest(null, url, method, token)
      .then(response => {
        this.regulations = this.getUserRegulations.getRegulations(response);
        console.log(this.regulations);

        this.spinner = false;

        setInterval( () => {
          this.nothingToShow = this.checkIfNothingToShow();
          this.noResults = this.checkIsNoResults();
          } , 500 );

        this.regulationsDataSource =
          new RegulationsDataSource(this.regulationsDB.setData(this.regulations));

        // init the auto complete
        this.filteredStates = this.stateCtrl.valueChanges
          .startWith(null)
          .map(state => state ? this.filterStates(state) : null);

        // Observable on search
        Observable.fromEvent(this.filter.nativeElement, 'keyup')
          .debounceTime(150)
          .distinctUntilChanged()
          .subscribe(() => {
            if (!this.regulationsDataSource) { return; }
            this.regulationsDataSource.filter = this.filter.nativeElement.value;
          });

        // this setting the defult view to library
        this.regulationsDataSource.viewChange = 'library';

        // Observable on year
        Observable.fromEvent(this.yearFilter.nativeElement, 'keyup')
          .debounceTime(150)
          .distinctUntilChanged()
          .subscribe(() => {
            if (!this.regulationsDataSource) { return; }
            this.regulationsDataSource.yearFilter = this.yearFilter.nativeElement.value;
          });

        // Observable on a_number
        Observable.fromEvent(this.aNumberFilter.nativeElement, 'keyup')
          .debounceTime(150)
          .distinctUntilChanged()
          .subscribe(() => {
            if (!this.regulationsDataSource) { return; }
            this.regulationsDataSource.a_numberFilter = this.aNumberFilter.nativeElement.value;
          });

        // Observable on a_number
        Observable.fromEvent(this.numberFilter.nativeElement, 'keyup')
          .debounceTime(150)
          .distinctUntilChanged()
          .subscribe(() => {
            if (!this.regulationsDataSource) { return; }
            this.regulationsDataSource.numberFilter = this.numberFilter.nativeElement.value;
          });
      })
      .catch(reject => {
        console.log(reject);
      });

  }

  public _animateFilters() {
    if (this._filtersAnimationState === 'hidden') {
      this._filtersAnimationState = 'shown';
    } else {
      this._filtersAnimationState = 'hidden';
    }
  }
  public fYear($val){
    if (!this.regulationsDataSource) { return; }
    this.regulationsDataSource.yearFilter = $val;
  }
  public  _animateSideNav() {
    if (this._sideNavAnimationState === 'hidden') {
      this._sideNavAnimationState = 'shown';
    } else {
      this._sideNavAnimationState = 'hidden';
    }
  }

  selectAll() {
    if ( ! this.allSelected) {
      this.allSelected = true;

      this.regulationsDataSource.filteredData.forEach(e => {
        this.selectedRegulationsObj[e.id] = e ;
      });

      this.selected = Object.keys(this.selectedRegulationsObj).length;

    }else {
      this.allSelected = false;
      this.selectedRegulationsObj = {};
      this.selected = 0;
    }
  }
}
