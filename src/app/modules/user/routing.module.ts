import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

// Components
import { RegulationComponent } from './regulation/regulation.component';
import { PreparePrintComponent } from './prepare-print/prepare-print.component';

// User  Components
import { UserComponent } from './user/user.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { UserResultsComponent } from './user-results/user-results.component';
import { UserPersonalComponent } from './user-personal/user-personal.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { UserQuestionsComponent } from './user-questions/user-questions.component';
import { UserActionsComponent } from './user-actions/user-actions.component';
import { PermitComponent } from './permit/permit.component';
import { NotFound404Component } from '../shared/not-found-404/not-found-404.component';
import { UserSearchComponent } from './user-search/user-search.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {path : '', component: UserComponent ,
            children: [
                {path: '', redirectTo: 'home', pathMatch: 'full'},
                {path: 'home', component: UserHomeComponent },
                // {path: 'permit/:permitID', component: PermitComponent },
                {path: 'regulation/:regulationID', component: RegulationComponent },
                {path: 'library', component: UserPersonalComponent},
                {path: 'results', component: UserResultsComponent},
                {path: 'questions', component: UserQuestionsComponent},
                {path: 'search', component: UserSearchComponent},
                {path: 'searching', component: SearchResultsComponent},
                {path: 'actions', component: UserActionsComponent},
                {path: 'print/:regulationID', component: PreparePrintComponent },
                {path: '**', component: NotFound404Component },
            ]},
          ])
    ],
    exports: [
        RouterModule
    ]
})
export class UserRoutingModule {
}

