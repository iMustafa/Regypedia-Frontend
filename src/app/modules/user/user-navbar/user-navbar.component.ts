import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { SigninComponent } from '../../../Pages-Authentication/sign-in.component';
import { TranslationService } from '../../../services/translation.service';
import { KeywordsService } from '../../../services/keywords.service';
import { CookiesService } from '../../../services/cookies.service';
import { ActionsService } from '../../../services/actions.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import {FormControl} from '@angular/forms';
import { MdDialog } from '@angular/material';
import { NotificationsComponent } from './notifications';
@Component({
  selector: 'app-user-navbar',
  templateUrl: './user-navbar.component.html',
  styleUrls: ['./user-navbar.component.css'],
  animations: [
    trigger('showNotifications', [
      state('hidden', style({
        'height': 0 ,
        'padding' : 0
      })),
      state('shown', style({
        'max-height': '100vh'
      })),
      transition('hidden <=> shown', animate('300ms ease-in-out'))
    ])
  ],
  providers: [ActionsService]
})
export class UserNavbarComponent implements OnInit {
  public notifications: any = [];
  public menuItems: any[] = [
    { 'title': '', 'routerLink': ['./home'] , 'key': 'mainPage' },
    { 'title': '', 'routerLink': ['./library'], 'key': 'library' },
    { 'title': '', 'routerLink': ['./questions'], 'key': 'questions' },
  ];
  public logoPath = '../../../assets/images/logo-white.png';
  public title = 'CMCSERV';
  public loginBtn = 'Login';
  public  stateCtrl: FormControl;
  public  filteredStates: Observable<any[]>;
  // public  _notificationAnimationState = 'hidden';
  public isDisabled = true;
  public isLoggedin: boolean;

  constructor(public auth: AuthenticationService,
    public  cookies: CookiesService,
    public translation: TranslationService,
    public keywords: KeywordsService,
    public actionService: ActionsService,
    public  router: Router,
    public dialog: MdDialog) {
    this.stateCtrl = new FormControl();
  }

    //  auto complete filter func
    public  filterAutoComplete(name: string) {
      if ( name !== '' && this.keywords.keywords_lib ) {
      return this.keywords.keywords_lib.filter(state =>
        state[this.translation.langKey].toLowerCase().indexOf(name.toLowerCase()) !== -1 );
      }else {
        return null;
      }
    }

    public  _logout() {
      this.cookies.setCookie('lang', null, 0);
      this.cookies.setCookie('api_token', null, 0);
      this.router.navigate(['/']);
    }

  changeLang() {
    this.translation.changeLangCookie();
  }

  showNotification() {
    return false;
  }


  ngOnInit() {
    this.actionService.getActions().then(response => {
      const that = this;
      response = JSON.parse(response._body);
      // tslint:disable-next-line:forin
      for (var key in response) {
        this.notifications.push(response[key]);
      }
      this.notifications.sort( (a,b)=> a.date_in_numbers >  b.date_in_numbers );
      return response;
    })
    .catch(reject => {
      return reject;
    });

    this.filteredStates = this.stateCtrl.valueChanges
    .startWith(null)
    .map(state => state ? this.filterAutoComplete(state) : null);

  }


  search(text , number = null , a_number = null , year = null) {
    this.router.navigate(
      [ '/user/search'],
      {queryParams : {text: text , number: number , a_number: a_number , year: year}}
    );
  }


  // public  _animateNotifications() {
  //   if (this._notificationAnimationState === 'hidden') {
  //     this._notificationAnimationState = 'shown';
  //   } else {
  //     this._notificationAnimationState = 'hidden';
  //   }
  // }


  canSearch( $value ) {
    if ($value === '') {
     this.isDisabled = true;
   }else {
     this.isDisabled = false;
    }
   }

   changeBackground() {
    if (this.cookies.getCookie('background') === '#2b2d31') {
      this.cookies.setCookie('background', '#efefef', 40);
    }else {
      this.cookies.setCookie('background', '#2b2d31', 40);
    }
    document.getElementById('body').style.background = this.cookies.getCookie('background');
   }


   changePassword() {
    const url     = 'https://panel.cmcserv.com/panel/api/v1/users/requestToUpdatePasswordByToken',
    cookie  = 'api_token',
    that = this;
    this.auth.submitData({} , url, cookie)
    .then(response => {
      this.router.navigate(['/resetpassword']);
      if (response) {
      return true;
      }
   }).catch();
  }


  openNotifications(): void {
    if (this.translation.innerWidth > 650) {
    const dialogRef = this.dialog.open(NotificationsComponent, {
      width: '40vw',
      height: '80vh',
      data: this.notifications,
    });
     }else {
      const dialogRef = this.dialog.open(NotificationsComponent, {
        width: '85vw',
        height: '80vh',
        data: this.notifications,
      });
     }
  }

}
