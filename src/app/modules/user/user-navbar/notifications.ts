import { Component , Inject } from '@angular/core';
import { MdDialog , MD_DIALOG_DATA } from '@angular/material';
import { TranslationService } from '../../../services/translation.service';
import {MdChipsModule} from '@angular/material';

@Component({
  selector: 'notifications-dialog',
  template: `
  <h3> {{lang.currentLang['notifications']}}: </h3>
  <h5 *ngIf="data.length < 0" style='font-size: 16px;' class="card-title" >A*A</h5>
  <md-card *ngFor='let notification of data' style='padding: 10px 10px;' [style.textAlign]="lang.textAlignValue" >
  <h5 style='font-size: 16px;' class="card-title" >{{(notification)?.regulation.name[lang.langKey]}}</h5>
  <p style='font-size: 14px;' >{{(notification)?.note}}</p>
  <span style='font-size: 10px;'>{{(notification)?.notifications}}</span>
</md-card>
<br>
 <button md-raised-button (click)="closeDialog()"> {{lang.currentLang['close']}} </button>
  `,
  styles: [ '.card-title{ text-align: inherit; }' , 'p{ text-align: inherit; }' ],
  providers: [TranslationService]
})
export class NotificationsComponent {


  constructor(public dialog: MdDialog, public lang: TranslationService ,
    @Inject(MD_DIALOG_DATA) public data: any) {

    }

  closeDialog() { this.dialog.closeAll(); }


}
