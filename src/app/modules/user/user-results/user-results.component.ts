import { Component, OnInit, AfterViewInit, Input, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { AuthorizationService } from '../../../services/authorization.service';
import { CookiesService } from '../../../services/cookies.service';
import { GetRegulationsService } from '../../../services/get-regulations.service';
import { TranslationService } from '../../../services/translation.service';
import { RegulationToolsService } from '../../../services/regulation-tools.service';
import { RegulationsDataSource, RegulationDB } from './classes';


import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

import * as jsPDF from 'jspdf';
import * as _ from 'lodash';
import { TimeInterval } from 'rxjs/operator/timeInterval';


@Component({
  selector: 'app-user-results',
  templateUrl: './user-results.component.html',
  styleUrls: ['./user-results.component.css'],
  providers: [AuthorizationService, GetRegulationsService , RegulationToolsService],
  animations: [
    trigger('showFilters', [
      state('hidden', style({
        'height': 0
      })),
      state('shown', style({
        'max-height': '20000px'
      })),
      transition('hidden <=> shown', animate('300ms ease-in-out'))
    ]),
    trigger('showSideNav', [
      state('hidden', style({
        'width' : 0
      })),
      state('shown', style({
        'width' : '20%'
      })),
      transition('hidden <=> shown', animate('300ms ease-in-out'))
    ])
  ]
})
export class UserResultsComponent implements AfterViewInit {

  @ViewChild('filter') filter: ElementRef;
  @ViewChild('showFavorite') showFavorite: ElementRef;
  @ViewChild('yearFilter') yearFilter: ElementRef;
  @ViewChild('numberFilter') numberFilter: ElementRef;
  @ViewChild('aNumberFilter') aNumberFilter: ElementRef;

  public displayedColumns: any[] = [ 'keyword' , 'type', 'number', 'year', 'a_number', 'id'];
  public spinner = true;
  public noResults = false;
  public nothingToShow = false;
  
  public regulations: any = [];
  public loaded: Boolean = false;
  public results: Boolean = false;

  public  _filtersAnimationState = 'hidden';
  public  _sideNavAnimationState = 'hidden';
  public  _view = 'regular';

  public selectedRegulationsObj: any = {};
  public selectedRegulationsArr: any = [];
  public checkBoxs: any = [];
  public selected= 0;
  public allSelected = false;
  public view = 1 ;
  public permits = [];

  public regulationsDB = new RegulationDB();
  public regulationsDataSource: RegulationsDataSource | null;

  constructor(public  authorize: AuthorizationService,
    public  translation: TranslationService,
    public  getUserRegulations: GetRegulationsService,
    public  regullationTools: RegulationToolsService) {
  }

  public  _updateAttr($reg , $state) {
    const data: Object = {
      regulation_id : $reg.id,
      state         : $state
    };
    const url = 'https://panel.cmcserv.com/panel/api/v1/user_regulation/updateAttributes';
    const method = 'Post';
    this.authorize.sendTokenizedRequest(data, url, method).then(response => {
      $reg.state = $state;
      // console.log(this.regulationsDB.data);
      // var index = this.regulationsDB.data.indexOf($reg);
      // if (index > -1) {
      //   this.regulationsDB.data.splice(index, 1);
      // }
      // console.log(this.regulationsDB.data);
    }).catch(reject => {
      console.log(reject);
    });
  }

  public  _showRow($reg) {
    console.log($reg);
    return true;
    // var regState = $reg.state ? $reg.state : null;
    // if (regState === this._view) {
    //   return true;
    // } else {
    //   return false;
    // }
  }


  public  _animateFilters() {
    if (this._filtersAnimationState === 'hidden') {
      this._filtersAnimationState = 'shown';
    } else {
      this._filtersAnimationState = 'hidden';
    }
  }



  checkIsNoResults() {
    return this.regulationsDataSource && this.regulationsDataSource.filteredData.length === 0
    && this.regulationsDataSource._exampleDatabase.data.length !== 0;
   }
   checkIfNothingToShow() {
     return this.regulationsDataSource && this.regulationsDataSource._exampleDatabase.data.length === 0;
   }

  ngAfterViewInit() {
    this.regulations = [];
    let url = 'https://panel.cmcserv.com/panel/api/v1/user_question/getAppliedRegulations';
    const method = 'Get';
    this.regulationsDB = new RegulationDB;
    this.authorize.sendTokenizedRequest(null, url, method)
      .then(response => {

        this.regulations = this.getUserRegulations.getRegulations(response);
        this.spinner = false;

        setInterval( () => {
          this.nothingToShow = this.checkIfNothingToShow();
          this.noResults = this.checkIsNoResults();
          } , 500 );

        this.loaded = !this.loaded;
        this.regulationsDataSource =
          new RegulationsDataSource(this.regulationsDB.setData(this.regulations));

        // Observable on search
        Observable.fromEvent(this.filter.nativeElement, 'keyup')
          .debounceTime(150)
          .distinctUntilChanged()
          .subscribe(() => {
            if (!this.regulationsDataSource) { return; }
            this.regulationsDataSource.filter = this.filter.nativeElement.value;
          });

        // Observable on year
        Observable.fromEvent(this.yearFilter.nativeElement, 'keyup')
          .debounceTime(150)
          .distinctUntilChanged()
          .subscribe(() => {
            if (!this.regulationsDataSource) { return; }
            this.regulationsDataSource.yearFilter = this.yearFilter.nativeElement.value;
          });
        // Observable on a_number
        Observable.fromEvent(this.aNumberFilter.nativeElement, 'keyup')
          .debounceTime(150)
          .distinctUntilChanged()
          .subscribe(() => {
            if (!this.regulationsDataSource) { return; }
            this.regulationsDataSource.a_numberFilter = this.aNumberFilter.nativeElement.value;
          });

        // Observable on a_number
        Observable.fromEvent(this.aNumberFilter.nativeElement, 'keyup')
        .debounceTime(150)
        .distinctUntilChanged()
        .subscribe(() => {
          if (!this.regulationsDataSource) { return; }
          this.regulationsDataSource.a_numberFilter = this.aNumberFilter.nativeElement.value;
        });

      // Observable on a_number
      Observable.fromEvent(this.numberFilter.nativeElement, 'keyup')
        .debounceTime(150)
        .distinctUntilChanged()
        .subscribe(() => {
          if (!this.regulationsDataSource) { return; }
          this.regulationsDataSource.numberFilter = this.numberFilter.nativeElement.value;
        });
      })
      .catch(reject => {
        console.log(reject);
      });
      url = 'https://panel.cmcserv.com/panel/api/v1/permit/getAppliedPermits/';
      this.authorize.sendTokenizedRequest( null , url , method).then( (res) => {
        res = JSON.parse( res['_body']);
        this.permits = res;
        this.permits.map(e=>{
          e.approval_authority = e.approval_authority || {en:'not available' , ar:'غير متاحة'};
          return e;
        });
      }).catch();

  }

  public _updateMultiAttr($state) {
    this.selectedRegulationsArr = [];
    this.selected = 0;
    this.allSelected = false;
    this.checkBoxs.forEach(element => {
      element.checked = false;
    });
    this.checkBoxs = [];
    // tslint:disable-next-line:forin
    for (var key in this.selectedRegulationsObj) {
      this.selectedRegulationsArr.push(key);
      this.selectedRegulationsObj[key].state = $state;
    }
    this.selectedRegulationsObj = {};
    var uniqueIDArray = _.uniq(this.selectedRegulationsArr);
    uniqueIDArray.forEach((id , index) => {
      var data: Object = {
        regulation_id: id,
        state: $state
      };
      var url: string = "https://panel.cmcserv.com/panel/api/v1/user_regulation/updateAttributes";
      var method: string = "Post";
      this.authorize.sendTokenizedRequest(data, url, method).then(response => {
        console.log('UPDATED!');
      }).catch(reject => {
        console.log(reject);
      });
    });
  }

  selectAll() {
    if ( ! this.allSelected) {
      this.allSelected = true;

      this.regulationsDataSource.filteredData.forEach(e => {
        this.selectedRegulationsObj[e.id] = e ;
      });
      this.selected = Object.keys(this.selectedRegulationsObj).length;

    }else {
      this.allSelected = false;
      this.selectedRegulationsObj = {};
      this.selected = 0;
    }
  }


  public selectRegulation($reg, $this) {
    this.checkBoxs.push($this);
    console.log($this);
    if (this.selectedRegulationsObj[$reg.id]) {
      delete this.selectedRegulationsObj[$reg.id];
      this.selected --;
    } else {
      this.selectedRegulationsObj[$reg.id] = $reg;
      this.selected ++;
    }
  }


  public print($regID) {
    this.regullationTools.printRegulation($regID).then(response => {
      console.log(response);
      if (response instanceof Array) {

      } else {
        var doc = new jsPDF();
        var regulation = `
        <!DOCTYPE html>
        <html lang="en">
          <head>
              <meta charset="UTF-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <meta http-equiv="X-UA-Compatible" content="ie=edge">
              <title>${response.regulation.name[this.translation.langKey]}</title>
              <style>
              .top-info {position: relative}
              .logo {width: 37px; height: 10px; float: left}
              .href {float: right; font-size: 12px; color: #333; display: inline-block; margin-top: 20px; text-align: right}
              .clearfix {clear: both}
              .text-center {text-align: center}
              .regulation-header {
                text-decoration: underline;
                display: inline-block;
                color: #444;
                border-bottom: 1px solid #EEE;
                padding: 0px 15px 4px 15px;
                margin: 0 0 5px 0;
                font-family: Roboto, Helvetica, Arial, sans-serif;
                font-size: 14px;
                font-weight: bold;
              }
              .regulation-info {display: block}
              .meta {float: left; text-align: center; width: 33.33333%}
              .meta strong { font-weight: 100; font-size: 13px; }
              .meta span { font-size: 13px; color: #555; font-style: italic; }
            </style>
          </head>
          <body>
            <secion class='top-info'>
              <img class='logo' src='http://localhost:4200/assets/images/logo-whitt.png' alt='Regypedia'>
              <span class='href'>www.panel.cmcserv.com</span>
              <div class='clearfix'></div>
            </section>
            <h1 class='text-center'>
              <span class='regulation-header'>${response.regulation.name[this.translation.langKey]}</span>
            </h1>
            </section>
            <secion class='regulation-info'>
              <div class="meta">
                <strong>${this.translation.en['regNum']}: </strong><span>${response.regulation.id}</span>
              </div>
              <div class="meta">
                <strong>${this.translation.en['regYear']}: </strong><span>${response.regulation.year}</span>
              </div>
              <div class="meta">
                <strong>${this.translation.en['regType']}: </strong><span>${response.regulation.degree}</span>
              </div>
              <div class="clearfix"></div>
            </section>
            <img src='https://panel.cmcserv.com/panel/public/${(response).text[0]}' alt='Regulation Image'>
          </body>
        </html>
        `;
        var imageHandler = {
          'IMG to be rendered out': function(element, renderer){
            return true;
         }
        }
        doc.fromHTML(regulation, 10, 10, {
          width: 100,
          imageHandler: imageHandler
        }, (image) => {
          doc.save(`regulation-id-${response.regulation.id}.pdf`)
        });
      }
    }).catch(reject => {
      console.log(reject);
    });
    // var doc = new jsPDF();
    // doc.text(20, 30, 'Hello World!');
    // doc.save('Regulation.pdf');
  }

  public printMultiRegulation() {
    this.selectedRegulationsArr = [];
    this.allSelected = false;
    this.selected = 0;
    this.checkBoxs.forEach(element => {
      element.checked = false;
    });
    this.checkBoxs = [];
    // tslint:disable-next-line:forin
    for (var key in this.selectedRegulationsObj) {
      this.selectedRegulationsArr.push(key);
    }
    this.selectedRegulationsObj = {};
    var uniqueIDArray = _.uniq(this.selectedRegulationsArr);
    this.regullationTools.printRegulation(uniqueIDArray).then(res => {
      var regulations = JSON.parse(res._body);
      for (var key in regulations) {
        var doc = new jsPDF();
        var regulation = `
        <!DOCTYPE html>
        <html lang="en">
          <head>
              <meta charset="UTF-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <meta http-equiv="X-UA-Compatible" content="ie=edge">
              <title>${regulations[key].name[this.translation.langKey]}</title>
              <style>
              .top-info {position: relative}
              .logo {width: 37px; height: 10px; float: left}
              .href {float: right; font-size: 12px; color: #333; display: inline-block; margin-top: 20px; text-align: right}
              .clearfix {clear: both}
              .text-center {text-align: center}
              .regulation-header {
                text-decoration: underline;
                display: inline-block;
                color: #444;
                border-bottom: 1px solid #EEE;
                padding: 0px 15px 4px 15px;
                margin: 0 0 5px 0;
                font-family: Roboto, Helvetica, Arial, sans-serif;
                font-size: 14px;
                font-weight: bold;
              }
              .regulation-info {display: block}
              .meta {float: left; text-align: center; width: 33.33333%}
              .meta strong { font-weight: 100; font-size: 13px; }
              .meta span { font-size: 13px; color: #555; font-style: italic; }
            </style>
          </head>
          <body>
            <secion class='top-info'>
              <img class='logo' src='http://localhost:4200/assets/images/logo-whitt.png' alt='Regypedia'>
              <span class='href'>www.panel.cmcserv.com</span>
              <div class='clearfix'></div>
            </section>
            <h1 class='text-center'>
              <span class='regulation-header'>${regulations[key].name[this.translation.langKey]}</span>
            </h1>
            </section>
            <secion class='regulation-info'>
              <div class="meta">
                <strong>${this.translation.en['regNum']}: </strong><span>${regulations[key].id}</span>
              </div>
              <div class="meta">
                <strong>${this.translation.en['regYear']}: </strong><span>${regulations[key].year}</span>
              </div>
              <div class="meta">
                <strong>${this.translation.en['regType']}: </strong><span>${regulations[key].degree}</span>
              </div>
              <div class="clearfix"></div>
            </section>
            <img src='https://panel.cmcserv.com/panel/public/${regulations[key].text_regulation[0]}' alt='Regulation Image'>
          </body>
        </html>
        `;
        var imageHandler = {
          'IMG to be rendered out': function (element, renderer) {
            return true;
          }
        }
        doc.fromHTML(regulation, 10, 10, {
          width: 100,
          imageHandler: imageHandler
        }, (image) => {
          doc.save(`regulation-id-${regulations[key].id}.pdf`)
        });
      }
    }).catch(reject => {
      console.log(reject);
    });
  }


}
