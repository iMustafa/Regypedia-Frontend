// Imports
// Angular Modules
import { SharedModule } from '../shared/shared.module';
import { animation } from '@angular/animations';
import { Http } from '@angular/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

// Services
import { UserRoutingModule } from './routing.module';
import { TranslationService } from '../../services/translation.service';
import { KeywordsService } from '../../services/keywords.service';
import { CookiesService } from '../../services/cookies.service';
import { AuthenticationService } from '../../services/authentication.service';
import { AuthorizationService } from '../../services/authorization.service';

// Directives
import { MatchHeightMaxDirective } from '../../directives/match-height-max.directive';

// Pipes
import { ReversePipe } from '../../pipes/reverse-array-pipe';
import { TruncatePipe } from '../../pipes/truncate.pipe';
import { NgxPaginationModule } from 'ngx-pagination';

// Material Design
import { MdProgressBarModule, MdTooltipModule, MdButtonToggleModule, MdMenuModule, MdTableModule,
MdPaginatorModule, MdSortModule} from '@angular/material';

import { CdkTableModule  } from '@angular/cdk/table';
import {MdAutocompleteModule} from '@angular/material';

// Components
import { UserComponent } from './user/user.component';
import { NotificationsComponent } from './user-navbar/notifications';

// User  Pages
import { UserHomeComponent } from './user-home/user-home.component';
import { UserSearchComponent , NewsDialogComponent } from './user-search/user-search.component';
import { UserResultsComponent } from './user-results/user-results.component';
import { UserPersonalComponent } from './user-personal/user-personal.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { UserQuestionsComponent } from './user-questions/user-questions.component';
import { UserActionsComponent } from './user-actions/user-actions.component';
import { UserNavbarComponent } from './user-navbar/user-navbar.component';
import { PermitComponent } from './permit/permit.component';
import { RegulationComponent } from './regulation/regulation.component';
import { PreparePrintComponent } from './prepare-print/prepare-print.component';
import { FooterComponent } from './footer/footer.component';
@NgModule({
    declarations: [
        UserComponent,
        NewsDialogComponent,
        // ConfirmPurchaseComponent,
        UserHomeComponent,
        UserSearchComponent,
        UserResultsComponent,
        UserPersonalComponent,
        // Declaring Pipes
        ReversePipe,
        // Declaring Directives
        MatchHeightMaxDirective,
        RegulationComponent,
        SearchResultsComponent,
        TruncatePipe,
        UserQuestionsComponent,
        UserActionsComponent,
        UserNavbarComponent,
        PermitComponent,
        PreparePrintComponent,
        FooterComponent,
        NotificationsComponent
    ],
    entryComponents: [
        NewsDialogComponent,
        NotificationsComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    imports: [
        CommonModule,
        UserRoutingModule,
        SharedModule,
        // Angular CDK Common
        CdkTableModule,
        NgxPaginationModule,
        MdProgressBarModule, MdTooltipModule, MdButtonToggleModule, MdMenuModule, MdTableModule,
        MdPaginatorModule, MdSortModule
    ],
    providers: [ KeywordsService ],
})
export class UserModule { }
