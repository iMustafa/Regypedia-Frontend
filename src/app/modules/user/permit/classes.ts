import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { MdPaginator, MdSort } from '@angular/material';


export interface RegulationData {
    id: string;
    name: string;
    number: number;
    a_number: number;
    point: number;
    year: number;
    type: string;
    text: string | null;
    state: string;
  }


  export class RegulationsDB {
    dataChange: BehaviorSubject<any> = new BehaviorSubject<any>([]);
    get data(): any { return this.dataChange.value; }


    setData($data) {
      const that = this;
      if ( $data.length === 0) {
        that.dataChange.next([]);
      }else {
      // tslint:disable-next-line:forin
        for ( const key in $data ) {
          const copiedData = that.data.slice();
          copiedData.push($data[key]);
          that.dataChange.next(copiedData);
        }
      }
      return this;
    }

}

  export class RegulationDataSource extends DataSource<any> {
    _filterChange = new BehaviorSubject('');
    get filter(): string { return this._filterChange.value; }
    set filter(filter: string) { this._filterChange.next(filter); }

     public filteredData: RegulationsDB[] = [];

    constructor(public  _exampleDatabase: RegulationsDB = null) {
      super();
    }



    connect(): Observable<RegulationsDB[]> {

        return Observable.of( this._exampleDatabase.data );

      }

    disconnect() {}

}
