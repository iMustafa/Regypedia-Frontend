import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthorizationService } from '../../../services/authorization.service';
import { TranslationService } from '../../../services/translation.service';
import { MdDatepickerModule, MdNativeDateModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { RegulationData , RegulationDataSource , RegulationsDB } from './classes';

@Component({
  selector: 'app-permit',
  templateUrl: './permit.component.html',
  styleUrls: ['./permit.component.css']
})
export class PermitComponent implements OnInit {
  public permitID: string;
  public primit: Object;
  public primit404 = false;
  displayedColumns = ['type', 'number', 'year' , 'name' , 'a_number' , 'tools'];
  dataSource: RegulationDataSource | null ;
  dataDB = new RegulationsDB();
  spinner = true;

  public url = 'https://panel.cmcserv.com/panel/public/';

  constructor(public translation: TranslationService, public _router: Router, public _route: ActivatedRoute
    , public authorize: AuthorizationService) {
    this.permitID = this._route.snapshot.params['permitID'];
  }

  ngOnInit() {
    let url = 'https://panel.cmcserv.com/panel/api/v1/permit/getById/' + this.permitID;
    this.authorize.sendTokenizedRequest (null , url , 'GET').then( (res) => {
      
      
        this.primit = JSON.parse(res['_body']);
        this.primit = this.extractPermit(this.primit);
      }).catch(reject => {
      console.log(reject);
      this._router.navigate(['/user/404']);
    });
    url = 'https://panel.cmcserv.com/panel/api/v1/permit/getRelated/' + this.permitID;
    this.authorize.sendTokenizedRequest(null , url , 'GET'). then(res => {
      res = JSON.parse(res['_body']);

      this.dataSource = new RegulationDataSource( this.dataDB.setData( res ) );
      this.spinner = false;
    }).catch( err => {
      console.log(err);
    });
  }

  extractPermit(primit) {
    primit['cost_attachments']['en'] =  primit['cost_attachments'][0].split( '$' ).filter( e  => e !== '' );
    primit['cost_attachments']['ar'] =  primit['cost_attachments'][1].split( '$' ).filter( e  => e !== '' );

    primit['required_documents_attachments']['en'] =  primit['required_documents_attachments'][0].split( '$' ).filter( e  => e !== '' );
    primit['required_documents_attachments']['ar'] =  primit['required_documents_attachments'][1].split( '$' ).filter( e  => e !== '' );
    primit['approval_authority'] = primit['approval_authority'] || {en:'not available' , ar:'غير متاحة'};
    return primit;
  }

}
