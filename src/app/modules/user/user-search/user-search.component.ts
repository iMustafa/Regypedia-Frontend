import { Component, OnInit , Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { AuthorizationService } from '../../../services/authorization.service';
import { SubmitVotesService } from '../../../services/submit-votes.service';
import { TranslationService } from '../../../services/translation.service';
import { KeywordsService } from '../../../services/keywords.service';
import { Router } from '@angular/router';
import {MdDialog, MdDialogRef, MD_DIALOG_DATA} from '@angular/material';
import {FormControl} from '@angular/forms';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['user-search.css'],
  animations: [
    trigger('showFilters', [
      state('hidden', style({
        'height': 0
      })),
      state('shown', style({
        'max-height': '300px'
      })),
      transition('hidden <=> shown', animate('300ms ease-in-out'))
    ]),
    trigger('showSideNav', [
      state('hidden', style({
        'width': '0'
      })),
      state('shown', style({
        'width': '40%'
      })),
      transition('hidden <=> shown', animate('300ms ease-in-out'))
    ])
  ],
  providers: [AuthorizationService, SubmitVotesService]
})
export class UserSearchComponent implements OnInit {

  public lodded = false;
  public posts: any[] = [];
  public votes: any[] = [];
  public shits: any[] = [];
  public isDisabled = true;
  public isDisabled2 = true;
  public voteImgPath = '../../assets/images/vote.png';
  public  stateCtrl: FormControl;
  public  filteredStates: Observable<any[]>;
  public  _filtersAnimationState = 'hidden';
  

  // tslint:disable-next-line:max-line-length
  constructor(public  authorize: AuthorizationService, public translation: TranslationService,
  public  submitVoteRequest: SubmitVotesService , public  router: Router ,
  public dialog: MdDialog , public keywords: KeywordsService ) {
    this.stateCtrl = new FormControl();
    this.keywords.getKeywords();
  }

  //  auto complete filter func
  public  filterAutoComplete(name: string) {
    if ( name !== '' && this.keywords.keywords_lib ) {
    return this.keywords.keywords_lib.filter(state =>
      state[this.translation.langKey].toLowerCase().indexOf(name.toLowerCase()) !== -1 );
    }else {
      return null;
    }
  }

  submitVote($vote, $answer) {
    $vote.submitting = true;
    this.submitVoteRequest.submitVote($vote.id, $answer).then(response => {
      console.log(response);
      $vote.submitted = true;
      setTimeout(function () {
        $vote.visible = false;
      }, 500);
    }).catch(reject => {
      console.log(reject);
    });
  }

  ngOnInit() {
    const pageUrl = 'https://panel.cmcserv.com/panel/api/v1/page',
          voteUrl = 'https://panel.cmcserv.com/panel/api/v1/user_vote/getUnAnswered',
          method  = 'Get',
          that = this;

    // PAGES AND VOTES REQUSTS ARE IN THE END OF THE FILE
    this.filteredStates = this.stateCtrl.valueChanges
    .startWith(null)
    .map(state => state ? this.filterAutoComplete(state) : null);

  }

  search( text , number, a_number , year , free = null) {
    console.log(text , number, a_number , year);
    this.router.navigate(
      [ '/user/searching/'],
      {queryParams : { text: text , number: number , a_number: a_number , year: year , free: free}}
    );
  }

  public _animateFilters() {
    if (this._filtersAnimationState === 'hidden') {
      this._filtersAnimationState = 'shown';
    } else {
      this._filtersAnimationState = 'hidden';
    }
  }
  searchEnter(searchText , yearFilter , aNumberFilter , numberFilter) {
    searchText =  searchText ? searchText :  '';
    yearFilter = yearFilter ? yearFilter : '';
    aNumberFilter = aNumberFilter ? aNumberFilter : '';
    numberFilter = numberFilter ? numberFilter : '';
    if ( this.canSearch( searchText + yearFilter + aNumberFilter + numberFilter)) {
      this.search(searchText + ' ', numberFilter , aNumberFilter , yearFilter);
    }
  }
  canSearch( $value ) {
   if ($value === '') {
    this.isDisabled = true;
    return false;
  }else {
    this.isDisabled = false;
    return true;
   }
  }
  canSearch2( $value ) {
    if ($value === '') {
     this.isDisabled2 = true;
   }else {
     this.isDisabled2 = false;
    }
   }

  // openDialog($news): void {
  //   const dialogRef = this.dialog.open(NewsDialogComponent, {
  //     data: $news
  //   });
  // }

}


// DAialog for Pages
@Component({
  selector: 'app-news-dialog',
  template: `
    <h3 style="text-align: center" > {{data.name}} </h3>
    <p>
      {{data.body}}
    </p>
  `,
  styles: ['img{ max-width:60%}' , ]
})
export class NewsDialogComponent {

  constructor(
    public dialogRef: MdDialogRef<NewsDialogComponent>,
    @Inject(MD_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}



  // // GET PAGES REQUEST
    // this.authorize.sendTokenizedRequest(null, pageUrl, method).then(response => {
    //   const posts = JSON.parse(response['_body']).data;
    //   posts.forEach(function (post) {
    //   console.log(post);
    //   that.posts.push(post);
    //   that.lodded = true;
    // });
    // }).catch(reject => {
    //   console.log(reject);
    // });

    // GET VOTES REQUEST
    // this.authorize.sendTokenizedRequest(null, voteUrl, method).then(response => {
    //   const votes = JSON.parse(response['_body']).data;
    //   console.log(votes);
    //   votes.forEach(function (vote) {
    //     vote.en.options = vote.en.options.split(',');
    //     vote.ar.options = vote.ar.options.split(',');
    //     vote.en.options.forEach(function (enOption, enIndex) {
    //       vote.ar.options.forEach(function (arOption, arIndex) {
    //         if (enIndex === arIndex) {
    //           const option = { value: { en: enOption, ar: arOption } };
    //           vote.en.options[enIndex] = { value: { en: enOption, ar: arOption }, title: vote.en.options[enIndex] };
    //           vote.ar.options[arIndex] = { value: { en: enOption, ar: arOption }, title: vote.ar.options[arIndex] };
    //         }
    //       });
    //     });
    //     vote['visible'] = true;
    //     vote['submitted'] = false;
    //     vote['identifier'] = vote['id'];
    //     that.votes.push(vote);
    //     console.log(vote);
    //   });
    // }).catch(reject => {
    //   console.log(reject);
    // });
