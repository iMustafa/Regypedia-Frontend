import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthorizationService } from '../../../services/authorization.service';
import { GetRegulationsService } from '../../../services/get-regulations.service';
import { TranslationService } from '../../../services/translation.service';
import { RegulationToolsService } from '../../../services/regulation-tools.service';

import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import * as _ from 'lodash';


@Component({
  selector: 'app-prepare-print',
  templateUrl: './prepare-print.component.html',
  styleUrls: ['./prepare-print.component.css'],
  providers: [
    AuthorizationService,
    GetRegulationsService,
    RegulationToolsService
  ]
})
export class PreparePrintComponent implements OnInit, AfterViewInit {

  @ViewChild('regulationRef') regulationElement;
  public regulation: any = {};
  public regulationID;
  public multiRegulations: any = [];
  public end = false;

  constructor(public router: Router,
    public route: ActivatedRoute,
    public authorize: AuthorizationService,
    public getUserRegulations: GetRegulationsService,
    public translation: TranslationService,
    public regullationTools: RegulationToolsService) {
    this.regulationID = this.route.snapshot.params['regulationID'];
  }

  public doc = new jsPDF('p', 'pt', 'a4');

  public _purchaseRegulation($ID): Promise<any> {
    const url = `https://panel.cmcserv.com/panel/api/v1/regulation/getText/${$ID}`,
      method = 'Get';
    return this.authorize.sendTokenizedRequest(null, url, method);
  }

  public print( $regID: any = null , $multi: Boolean = false , $final: Boolean = false): Promise<any> {
    if ($regID) {
      this.regulationID = $regID;
    }
    if ( !this.end ) {
      console.log('hi' , this.regulationID);

    return this.regullationTools.printRegulation(this.regulationID).then(res => {
      const response = JSON.parse(res._body).data;
      this.multiRegulations.push(response);
      const imageHandler = {
        'IMG to be rendered out': function (element, renderer) {
          return true;
        }
      };
      const lastIndex = this.regulationElement.nativeElement.children.length;
        this.doc.addHTML(this.multiRegulations[this.regulationElement.nativeElement.children[lastIndex]], 0, 0, {
          background: '#FFF'
        }, (print) => {
          if (!$multi && !$final) {
            // console.log('Single');
            // this.doc.save(`regulation-id-${response.regulation.id}.pdf`);
          } else {
            if (!$final) {
              console.log('Add Page');
              this.doc.addPage();
            } else {
              console.log('Final');
              this.end = true;
              console.log(this.multiRegulations);

              // this.doc.save('regulations.pdf');
              setTimeout(() => {
                // close();
                this.printing();
              }, 200);
            }
          }
        });
    }).catch(reject => {
      console.log(reject);
    });
  }

  return;

}

  ngOnInit() {
    const that = this;
    const regulationIdsArr = that.regulationID.split(',');

    function getRegulation($multi: Boolean = false, $final: Boolean = false, i: number) {

      if ($final) {
        return ;
      }

      if (i === regulationIdsArr.length - 1) {
        $final = true;
      }

      that.print(regulationIdsArr[i], $multi, $final).then(printed => {
        getRegulation($multi, $final, ++i);
      }).catch(reject => {
        console.log(reject);
      });

    }

    if (regulationIdsArr.length > 1) {
      getRegulation(true, false, 0);
    } else {
      getRegulation(false, false, 0);
    }
  }

  ngAfterViewInit() { }


  printing () {

    window.print();

  }

}
