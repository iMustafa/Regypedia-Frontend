import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';

import { NotFound404Component } from './not-found-404/not-found-404.component';
import { CmcFooterComponent } from './cmc-footer/cmc-footer.component';
import { ParseObjPipe } from '../../pipes/parse-obj.pipe';
import { HttpModule } from '@angular/http';
// Md Modules
// Material Design
import { MdProgressBarModule, MdTooltipModule, MdMenuModule,
  MdTabsModule, MdCardModule, MdButtonModule, MdInputModule, MdDialogModule,
  MdIconModule, MdProgressSpinnerModule, MdCheckboxModule } from '@angular/material';
import {MdRadioModule} from '@angular/material';
import {MdDatepickerModule , MdNativeDateModule} from '@angular/material';
import {MdToolbarModule} from '@angular/material';
import {MdSelectModule} from '@angular/material';
import {MdListModule} from '@angular/material';
import {MdAutocompleteModule} from '@angular/material';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    HttpModule,
    // Material Design
    MdTabsModule,
    MdCardModule,
    MdInputModule,
    MdButtonModule,
    MdDialogModule,
    MdIconModule,
    MdProgressSpinnerModule,
    MdRadioModule,
    MdCheckboxModule,
    MdProgressBarModule,
    MdMenuModule,
    MdTooltipModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MdAutocompleteModule,
    MdToolbarModule,
    MdSelectModule,
    MdListModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ParseObjPipe,
    NotFound404Component,
    CmcFooterComponent
  ],
  exports: [
    ParseObjPipe,
    NotFound404Component,
    HttpModule,
    MdTabsModule,
    MdCardModule,
    MdInputModule,
    MdButtonModule,
    MdDialogModule,
    MdIconModule,
    MdProgressSpinnerModule,
    MdRadioModule,
    MdCheckboxModule,
    MdProgressBarModule,
    MdMenuModule,
    MdTooltipModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MdAutocompleteModule,
    MdToolbarModule,
    MdSelectModule,
    MdListModule,
    FormsModule,
    ReactiveFormsModule,
    CmcFooterComponent
  ]
})
export class SharedModule { }
