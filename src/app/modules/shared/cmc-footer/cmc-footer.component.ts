import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cmc-footer',
  templateUrl: './cmc-footer.component.html',
  styleUrls: ['./cmc-footer.component.css']
})
export class CmcFooterComponent implements OnInit {

  constructor() { }
  items = [
    {
      name: 'الرئيسية',
    },
    {
      name: 'خدماتنا',
    },
    {
      name: 'من نحن',
    },
    {
      name: 'تواصل معنا',
    }
  ]

  icons = [
    {
      src: 'assets/images/icons/icon-06.png',
      url: 'https://www.linkedin.com/company/27153813/'
    },
    {
      src: 'assets/images/icons/icon-05.png',
      url: 'https://www.facebook.com/CMCSERVCOM/'      
    },
    {
      src: 'assets/images/icons/icon-04.png',
      url: 'sendTo:info@cmcserv.com'
    }
  ]
  ngOnInit() {
  }

}
