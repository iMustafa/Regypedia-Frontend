import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../../services/translation.service';
@Component({
  selector: 'app-not-found-404',
  templateUrl: './not-found-404.component.html',
  styleUrls: ['./not-found-404.component.css'],
})
export class NotFound404Component implements OnInit {

  constructor( public  translation: TranslationService) { }

  ngOnInit() {
  }

}
