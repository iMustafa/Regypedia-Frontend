import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'parseObj'
})
export class ParseObjPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let valueObj = JSON.parse(value) ? JSON.parse(value) : {};
    var returnValue;
    
    if (valueObj[args]) {
      returnValue = valueObj[args];
    }

    return returnValue;
  }

}
