import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

// Components
import { HomeComponent } from './components/home/home.component';

// Authentication Components
import { SignUpComponent } from './Pages-Authentication/sign-up.component';
import { ForgetPasswordComponent } from './Pages-Authentication/forget-password.component';
import { NotFound404Component } from './modules/shared/not-found-404/not-found-404.component';
import { ResetPasswordComponent } from './Pages-Authentication/reset-password/reset-password.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
          { path : '', component: HomeComponent },
          { path:  'sign-up', component: SignUpComponent },
          { path:  'forget-password', component: ForgetPasswordComponent },
          { path:  'resetpassword', component: ResetPasswordComponent },
          { path:  'forget-password', component: ForgetPasswordComponent },
          { path:  'user', loadChildren: './modules/user/user.module#UserModule' },
          {path: '**', component: NotFound404Component },
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
