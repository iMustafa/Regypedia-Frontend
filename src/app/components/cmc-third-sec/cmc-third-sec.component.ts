import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../services/translation.service';
import { TextService } from '../../services/text.service';
@Component({
  selector: 'cmc-third-sec',
  templateUrl: './cmc-third-sec.component.html',
  styleUrls: ['./cmc-third-sec.component.css'],
  providers: [ TextService]
})
export class CmcThirdSecComponent implements OnInit {

  public secText= '';
  constructor( public translation: TranslationService , public text: TextService ) { }

  ngOnInit() {
    this.text.getAboutusSecText().then( res => {
      console.log(res);
      this.secText = res;
    }).catch();
  }

}
