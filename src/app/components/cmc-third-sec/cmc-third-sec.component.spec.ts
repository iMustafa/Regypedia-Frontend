import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmcThirdSecComponent } from './cmc-third-sec.component';

describe('CmcThirdSecComponent', () => {
  let component: CmcThirdSecComponent;
  let fixture: ComponentFixture<CmcThirdSecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmcThirdSecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmcThirdSecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
