import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { SigninComponent } from '../../Pages-Authentication/sign-in.component';
import { TranslationService } from '../../services/translation.service';
import { CookiesService } from '../../services/cookies.service';
import { AuthenticationService } from '../../services/authentication.service';
import { ActionsService } from '../../services/actions.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-home-page-navbar',
  templateUrl: './home-page-navbar.component.html',
  styleUrls: ['./home-page-navbar.component.css'] ,
  animations: [
    trigger('showNotifications', [
      state('hidden', style({
        'height': 0
      })),
      state('shown', style({
        'max-height': '150px'
      })),
      transition('hidden <=> shown', animate('300ms ease-in-out'))
    ])
  ],
  providers: [MdDialog, SigninComponent, AuthenticationService, ActionsService ]
})
export class HomePageNavbarComponent implements OnInit {

  public logoPath = '../../assets/images/logo-whitt.png';
  public title = 'CMCserv';
  public loginBtn = 'Login';
  public langBtn = 'EN';
  public  lang: any = {};
  public  stateCtrl: FormControl;
  public  filteredStates: Observable<any[]>;
  public  _notificationAnimationState = 'hidden';
  public isDisabled = true;
  public isLoggedin: boolean;
  public notifications: any = [];

  constructor(public auth: AuthenticationService,
    public  cookies: CookiesService,
    public translation: TranslationService,
    public dialog: MdDialog ,
    public actionService: ActionsService,
    public  router: Router) {
    this.renderLang(this);
    this.stateCtrl = new FormControl();
  }

  public showSection(): Boolean {
    return this.isLoggedin;
  }


  public  _logout() {
    this.cookies.setCookie('lang', null, 0);
    this.cookies.setCookie('api_token', null, 0);
    this.router.navigate(['/']);
  }

  public viewProfile() {
    this.router.navigate(['/user/']);
  }

  openDialog() {
    if (!this.auth.isLoggedIn()) {
      this.dialog.open(SigninComponent);
    }
  }
  closeDialog() { this.dialog.closeAll(); }

  changeLang() {
    this.translation.changeLangCookie();
    this.renderLang(this);
  }

  showNotification() {
    return false;
  }


  ngOnInit() {
    if ( this.auth.isLoggedIn() ) {
    this.actionService.getActions().then(response => {
      var that = this;
      response = JSON.parse(response._body);
      // tslint:disable-next-line:forin
      for (let key in response) {
        this.notifications.push(response[key]);
      }
      return response;
    })
    .catch(reject => {
      return reject;
    });
   }
  }

  renderLang($this) {
    const that = $this;
    if (this.cookies.getCookie('lang') === 'en') {
      this.lang = this.translation.en;
      this.langBtn = this.translation.en['langBtn'];
      this.loginBtn = this.translation.en['userLogin'];

    } else if (this.cookies.getCookie('lang') === 'ar') {
      this.lang = this.translation.ar;
      this.langBtn = this.translation.ar['langBtn'];
      this.loginBtn = this.translation.ar['userLogin'];
    }
  }

  search(text , number = null , a_number = null , year = null) {
    this.router.navigate(
      [ '/profile', {'outlets': {'view': ['search']} } ],
      {queryParams : {text: text , number: number , a_number: a_number , year: year}}
    );
  }


  public  _animateNotifications() {
    if (this._notificationAnimationState === 'hidden') {
      this._notificationAnimationState = 'shown';
    } else {
      this._notificationAnimationState = 'hidden';
    }
  }


  canSearch( $value ) {
    if ($value === '') {
     this.isDisabled = true;
   }else {
     this.isDisabled = false;
    }
   }

}

