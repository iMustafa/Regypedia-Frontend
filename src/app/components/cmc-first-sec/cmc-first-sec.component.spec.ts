import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmcFirstSecComponent } from './cmc-first-sec.component';

describe('CmcFirstSecComponent', () => {
  let component: CmcFirstSecComponent;
  let fixture: ComponentFixture<CmcFirstSecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmcFirstSecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmcFirstSecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
