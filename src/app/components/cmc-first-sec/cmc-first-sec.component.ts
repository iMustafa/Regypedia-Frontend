import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { SignUpComponent } from '../../Pages-Authentication/sign-up.component';
import { TranslationService } from '../../services/translation.service';
import { TextService } from '../../services/text.service';
@Component({
  selector: 'cmc-first-sec',
  templateUrl: './cmc-first-sec.component.html',
  styleUrls: ['./cmc-first-sec.component.css'],
  providers: [ TextService ]
})
export class CmcFirstSecComponent implements OnInit {
  public secText= '';
  public textloaded= false;
  constructor( public dialog: MdDialog , public lang: TranslationService, public text: TextService ) {

  }

  ngOnInit() {
    this.text.getFirstSecText().then( res => {
      console.log(res);
      this.secText = res;
      this.textloaded=true;
    }).catch();
  }

  demo() {
    this.dialog.open(SignUpComponent);
  }
}
