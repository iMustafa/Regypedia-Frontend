import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmcFooterComponent } from './cmc-footer.component';

describe('CmcFooterComponent', () => {
  let component: CmcFooterComponent;
  let fixture: ComponentFixture<CmcFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmcFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmcFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
