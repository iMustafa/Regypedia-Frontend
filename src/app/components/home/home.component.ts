import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../services/translation.service';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( public translation: TranslationService , public auth: AuthenticationService , 
    public router: Router) { 
      if (this.auth.isLoggedIn()) {
        this.router.navigate(['/user']);
      }
    }

  ngOnInit() {
  }

}
