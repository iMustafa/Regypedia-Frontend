import { Component, OnInit } from '@angular/core';
import {  TranslationService } from '../../services/translation.service';
import {  AuthenticationService } from '../../services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'cmc-fourth-sec',
  templateUrl: './cmc-fourth-sec.component.html',
  styleUrls: ['./cmc-fourth-sec.component.css'],
  providers: [ AuthenticationService]
})
export class CmcFourthSecComponent implements OnInit {
  public  msgForm: FormGroup;
  public sent = false;
  constructor( public lang: TranslationService , public  fb: FormBuilder ,public auth: AuthenticationService) {
    this.msgForm = fb.group({
      'email': [null, Validators.compose([Validators.email, Validators.required])],
      'name': [null, Validators.required ],
      'phone': [null, Validators.required ],
      'message': [null, Validators.required ],
    });
   }

  ngOnInit() {
  }

  submitForm($formData) {
    const url     = 'https://panel.cmcserv.com/panel/api/v1/users/sendMail';
    this.auth.unauthorized($formData, url)
    .then(response => {
      console.log(response);
      if (response) {
        this.sent = true;
      } 
    })
    .catch(reject => {
    });
  }


}
