import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmcFourthSecComponent } from './cmc-fourth-sec.component';

describe('CmcFourthSecComponent', () => {
  let component: CmcFourthSecComponent;
  let fixture: ComponentFixture<CmcFourthSecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmcFourthSecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmcFourthSecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
