import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmcSecondSecComponent } from './cmc-second-sec.component';

describe('CmcSecondSecComponent', () => {
  let component: CmcSecondSecComponent;
  let fixture: ComponentFixture<CmcSecondSecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmcSecondSecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmcSecondSecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
