import { Component, OnInit } from '@angular/core';
import { TextService } from '../../services/text.service';
import { TranslationService } from '../../services/translation.service';

@Component({
  selector: 'cmc-second-sec',
  templateUrl: './cmc-second-sec.component.html',
  styleUrls: ['./cmc-second-sec.component.css'],
  providers: [ TextService]
})
export class CmcSecondSecComponent implements OnInit {

  constructor( public lang: TranslationService , public text: TextService ) { }

  public services = [];

  ngOnInit() {
    this.text.getServices().then(res => {
      this.services = res.filter((e)=> e.id <= 11 );
      console.log(res);
    });

  }

}
