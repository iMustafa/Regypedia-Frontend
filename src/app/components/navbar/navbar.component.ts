import { Component, OnInit } from '@angular/core';
import { TranslationService } from '../../services/translation.service';
import { AuthenticationService } from '../../services/authentication.service';
import { MdDialog } from '@angular/material';
import { SigninComponent } from '../../Pages-Authentication/sign-in.component';
import {PageScrollConfig} from 'ngx-page-scroll';


@Component({
  selector: 'cmc-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor( public translation: TranslationService,
    public dialog: MdDialog ,
    public auth: AuthenticationService) {
  }

  items = [
    {
      name: 'mainPage',
      href: '#mainPage'
    },
    {
      name: 'services',
      href: '#services'
    },
    {
      name: 'aboutUs',
      href: '#aboutUs'
    },
    {
      name: 'contactUs',
      href: '#contactUs'
    }
  ];

  changeLang() {
    this.translation.changeLangCookie();
  }


  openDialog() {
    if (!this.auth.isLoggedIn()) {
      this.dialog.open(SigninComponent);
    }
  }

  ngOnInit() {

  }

}
