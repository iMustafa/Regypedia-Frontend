// Imports
// Angular Modules
import { AppComponent } from './app.component';
import { SharedModule } from './modules/shared/shared.module';
import { animation } from '@angular/animations';
import { Http } from '@angular/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { Router} from '@angular/router';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Services
import { AppRoutingModule } from './routing.module';
import { TranslationService } from './services/translation.service';
import { CookiesService } from './services/cookies.service';
import { AuthenticationService } from './services/authentication.service';
import { AuthorizationService } from './services/authorization.service';
import { ResetPasswordComponent } from './Pages-Authentication/reset-password/reset-password.component';


// Components
import { HomePageNavbarComponent } from './components/home-page-navbar/home-page-navbar.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CmcFooterComponent } from './modules/shared/cmc-footer/cmc-footer.component';
import { CmcFirstSecComponent } from './components/cmc-first-sec/cmc-first-sec.component';
import { CmcSecondSecComponent } from './components/cmc-second-sec/cmc-second-sec.component';
import { CmcThirdSecComponent } from './components/cmc-third-sec/cmc-third-sec.component';
import { CmcFourthSecComponent } from './components/cmc-fourth-sec/cmc-fourth-sec.component';


// Authentication Pages
import { SigninComponent } from './Pages-Authentication/sign-in.component';
import { SignUpComponent } from './Pages-Authentication/sign-up.component';
import { ForgetPasswordComponent } from './Pages-Authentication/forget-password.component';
import { PageScroll , PageScrollService } from 'ngx-page-scroll';

@NgModule({
    declarations: [
        AppComponent,
        SignUpComponent,
        SigninComponent,
        // Declaring Directives
        ForgetPasswordComponent,
        HomeComponent,
        // HomePageComponent,
        // HomePageHeaderComponent,
         HomePageNavbarComponent,
         NavbarComponent,
         CmcFirstSecComponent,
         CmcSecondSecComponent,
         CmcThirdSecComponent,
         CmcFourthSecComponent,
         PageScroll,
         ResetPasswordComponent
    ],
    entryComponents: [
        SigninComponent,
    ],
    imports: [
        CommonModule,
        AppRoutingModule,
        SharedModule,
        BrowserAnimationsModule,
    ],
    providers: [ TranslationService , CookiesService, AuthenticationService, AuthorizationService , PageScrollService],
    bootstrap: [AppComponent]
})
export class AppModule { }
