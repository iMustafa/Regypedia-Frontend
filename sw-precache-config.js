module.exports = {
  navigateFallback: '/index.html',
    stripPrefix: 'dist',
    root: 'dist/',
    staticFileGlobs: [
      'dist/index.html',
      'dist/**'
    ],
    runtimeCaching: [{
      "urlPattern": "panel.cmcserv.com/panel/api*",
      "handler": "fastest"
    }]  
  };